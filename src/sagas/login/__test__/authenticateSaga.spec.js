import { expectSaga } from 'redux-saga-test-plan';
import * as Api from '@api';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import authenticateSaga from '../authenticateSaga';
import { authenticate, setLoginError } from '@actions/loginActionCreator';
import { setIsLoggedIn, setIsUserVerified } from '@actions/appStateActionCreator';
import { setCurrentUser, setAuthToken } from '@actions/currentUserActionCreator';
import { setVerificationCode } from '@actions/verificationActionCreator';

describe('authenticate saga', () => {
  describe('credentials are correct and user is verified', () => {
    it('should set token and user and isLoggedIn and isVerified correctly', async () => {
      const creds = { usernameEmail: 'abcd', password: '1234' };
      const user = { verifyStatus: true };
      const token = 123456;
      return expectSaga(authenticateSaga)
        .provide([
          [
            matchers.call.fn(Api.authenticate),
            { data: { user, token } },
          ],
        ])
        .put(setAuthToken(token))
        .put(setCurrentUser(user))
        .call(Api.storeToken, token)
        .put(setIsLoggedIn(true))
        .put(setIsUserVerified(true))
        .dispatch(authenticate(creds))
        .run();
    });
  });
  describe('credentials are correct but user is not verified', () => {
    it('should set token and user and isLoggedIn then fetch verifyCode and set it', async () => {
      const creds = { usernameEmail: 'abcd', password: '1234' };
      const user = { verifyStatus: false };
      const token = 123456;
      const verifyCode = 987654;

      return expectSaga(authenticateSaga)
        .provide([
          [
            matchers.call.fn(Api.authenticate),
            { data: { user, token } },
          ],
          [
            matchers.call.fn(Api.resendVerifyEmail),
            { data: { code: verifyCode } },
          ],
        ])
        .put(setAuthToken(token))
        .put(setCurrentUser(user))
        .call(Api.storeToken, token)
        .put(setIsLoggedIn(true))
        .put(setIsUserVerified(false))
        .put(setVerificationCode(verifyCode))
        .dispatch(authenticate(creds))
        .run();
    });
  });
  describe('credentials are wrong', () => {
    it('should set login error', async () => {
      const creds = { usernameEmail: 'abcd', password: '1234' };

      const errorMessage = 'some error';

      return expectSaga(authenticateSaga)
        .provide([
          [
            matchers.call.fn(Api.authenticate),
            throwError({ data: { detail: errorMessage } }),
          ],
        ])
        .put(setLoginError(errorMessage))
        .not.put(setIsLoggedIn(true))
        .dispatch(authenticate(creds))
        .run();
    });
  });
});
