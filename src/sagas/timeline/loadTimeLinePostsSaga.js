import {
  takeLatest, call, put, select,
} from 'redux-saga/effects';
import { loadTimelinePosts, setTimelinePosts, setTimelineNextPageUrl } from '@actions/timeLineActionCreator';
import * as Api from '@api';

// not finished
function* worker() {
  try {
    // console.log('load called');

    const { id: userId } = yield select((state) => state.currentUser.currentUser);
    // console.log('load user', userId);
    const {
      data: {
        // eslint-disable-next-line no-unused-vars
        count, next, previous, results,
      },
    } = yield call(Api.getUsersTimeline, userId);
    // console.log('load', results);

    yield put(setTimelinePosts(results));
    yield put(setTimelineNextPageUrl(next));
  } catch (e) {
    console.log('error', e);
  }
}

export default function* loadTimelinePostsSaga() {
  yield takeLatest(loadTimelinePosts, worker);
}
