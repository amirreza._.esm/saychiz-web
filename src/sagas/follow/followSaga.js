/* eslint-disable camelcase */
import {
  put, call, takeLatest,
} from 'redux-saga/effects';
import * as Api from '@utils/Api';
import { followUser } from '@actions/followActionCreator';
import { addCurrentUserFollowing } from '@actions/currentUserActionCreator';

function* worker({ payload }) {
  try {
    const {
      data: {
        following_user,
      },
    } = yield call(Api.follow, payload.visitedUser, payload.currentUser);
    yield put(addCurrentUserFollowing(following_user));
  } catch (e) {
    console.log('delete error');
  }
}

export default function* followSaga() {
  yield takeLatest(followUser, worker);
}
