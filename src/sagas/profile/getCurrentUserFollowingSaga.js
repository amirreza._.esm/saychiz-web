import {
  put, call, takeLatest,
} from 'redux-saga/effects';
import * as Api from '../../utils/Api';
import {
  addCurrentUserFollowing, getAllCurrentUserFollowing, setCurrentUserFollowing,
} from '../../actions/currentUserActionCreator';

function* worker({ payload }) {
  try {
    let { data: { next, previous, results } } = yield call(Api.getUserslist, payload);
    yield put(setCurrentUserFollowing(results));
    while (next) {
      const xxx = yield call(Api.getUserslist, next);
      next = xxx.data.next;
      previous = xxx.data.previous;
      results = xxx.data.results;
      if (previous) {
        yield put(addCurrentUserFollowing(results));
      } else {
        yield put(setCurrentUserFollowing(results));
      }
    }
  } catch (e) {
    console.log('error');
  }
}

export default function* getCurrnetUserFollowingSaga() {
  yield takeLatest(getAllCurrentUserFollowing, worker);
}