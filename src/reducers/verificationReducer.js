import { handleActions } from 'redux-actions';
import { setVerificationCode, setVerificationError, setVerificationPending } from '@actions/verificationActionCreator';

const initialState = {
  verificationCode: null,
  error: null,
  isPending: false,
};

export default handleActions({
  [setVerificationCode]: (state, { payload }) => ({ ...state, verificationCode: payload }),
  [setVerificationError]: (state, { payload }) => ({ ...state, error: payload }),
  [setVerificationPending]: (state, { payload }) => ({ ...state, isPending: payload }),
}, initialState);
