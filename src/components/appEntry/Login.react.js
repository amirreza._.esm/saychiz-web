import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Button, Typography, Box, Grid,
} from '@material-ui/core';
import { authenticate } from '@actions/loginActionCreator';
import NeuTextField from '../neumorphism/NeuTextField.react';
import NeuButton from '../neumorphism/NeuButton.react';
import ThirdPartyAuth from './ThirdPartyAuth.react';

const Login = () => {
  const dispatch = useDispatch();

  const error = useSelector((state) => state.login.error);
  const isPending = useSelector((state) => state.login.isPending);

  const [formValues, setFormValues] = useState({
    usernameEmail: '',
    password: '',
  });

  const handleFormChange = (prop) => (e) => setFormValues(
    { ...formValues, [prop]: e.target.value },
  );

  const handleSubmit = () => {
    dispatch(authenticate(
      { usernameEmail: formValues.usernameEmail, password: formValues.password },
    ));
  };

  return (
    <Box
      minWidth={{ xs: 300 }}
      clone
      onKeyPress={(e) => { if (e.key === 'Enter') handleSubmit(); }}
    >
      <Grid container spacing={2} direction="column">
        <Grid item container justify="center">
          <ThirdPartyAuth />
        </Grid>
        <Grid item>
          <NeuTextField
            label="Username or email"
            fullWidth
            margin="dense"
            value={formValues.usernameEmail}
            onChange={handleFormChange('usernameEmail')}
            error={!!error}
          />
        </Grid>
        <Grid item>
          <NeuTextField
            label="Password"
            type="password"
            fullWidth
            margin="dense"
            value={formValues.password}
            onChange={handleFormChange('password')}
            error={!!error}
          />
        </Grid>
        <Grid item>
          <Typography
            color={error ? 'error' : 'initial'}
          >
            {error}

          </Typography>
        </Grid>
        <Grid item container justify="center">
          <Button variant="text" color="default">Forgot your password?</Button>
        </Grid>
        <Grid item container justify="center">
          <NeuButton
            pending={isPending}
            textColor="primary"
            onClick={handleSubmit}
          >
            Go!
          </NeuButton>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Login;
