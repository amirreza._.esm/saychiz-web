import React from 'react';
import {
  Button, makeStyles, useTheme, CircularProgress,
} from '@material-ui/core';
import Color from 'color';
import * as PropTypes from 'prop-types';


const useStyles = makeStyles(() => ({
  root: (props) => ({
    ...(props.textColor && { color: props.textColor }),
    boxShadow: `${props.isInset ? 'inset' : ''} -${props.offset}px -${props.offset}px ${props.spread}px ${props.lightColor}
        , ${props.isInset ? 'inset' : ''} ${props.offset}px ${props.offset}px ${props.spread}px ${props.darkColor}`,
    borderRadius: `${props.borderRadius}px`,
    backgroundColor: props.color,
    '&:hover': {
      ...(props.hoverTextColor && { color: props.hoverTextColor }),
      boxShadow: `${props.isInset ? 'inset' : ''} -${props.hoverOffset * 2}px -${props.hoverOffset * 2}px ${props.hoverSpread}px ${props.lightColor}
          , ${props.isInset ? 'inset' : ''} ${props.hoverOffset * 2}px ${props.hoverOffset * 2}px ${props.hoverSpread}px ${props.darkColor}`,
      borderRadius: `${props.borderRadius}px`,
      backgroundColor: props.color,
    },
    '&:active': {
      boxShadow: 'none',
      borderRadius: `${props.borderRadius}px`,
      backgroundColor: props.color,
    },
    '&:focus': {
      boxShadow: `${props.isInset ? 'inset' : ''} -${props.offset * 1.5}px -${props.offset * 1.5}px ${props.spread}px ${props.lightColor}
          , ${props.isInset ? 'inset' : ''} ${props.offset * 1.5}px ${props.offset * 1.5}px ${props.spread}px ${props.darkColor}`,
      borderRadius: `${props.borderRadius}px`,
      backgroundColor: props.color,
    },
  }),
  wrapper: (props) => ({
    position: 'relative',
    borderRadius: `${props.borderRadius}px`,
  }),
  progress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

const NeuButton = ({
  elevation = 1, hoverElevation, borderRadius = 1, textColor, hoverTextColor, pending, ...otherProps
}) => {
  const theme = useTheme();
  const isInset = elevation < 0;
  const elevationFactor = Math.abs(elevation);
  const offset = elevationFactor * 2;
  const spread = elevationFactor * 6;
  const hoverOffset = hoverElevation ? hoverElevation * 2 : elevationFactor * 2;
  const hoverSpread = hoverElevation ? hoverElevation * 6 : elevationFactor * 6;
  const color = theme.palette[otherProps.color]?.main || theme.palette.background.default;
  const lightColor = new Color(color).lighten(0.8).fade(0.1).rgb()
    .string();
  const darkColor = new Color(color).darken(0.6).fade(0.6).rgb()
    .string();

  const classes = useStyles({
    isInset,
    borderRadius: (theme.shape.borderRadius * borderRadius * 2),
    offset,
    spread,
    hoverOffset,
    hoverSpread,
    lightColor,
    color,
    darkColor,
    textColor: theme.palette[textColor]?.main || textColor?.(theme),
    hoverTextColor: theme.palette[hoverTextColor]?.main || hoverTextColor?.(theme),
  });

  const Btn = () => (pending ? (
    <div className={classes.wrapper}>
      <Button
        classes={{
          root: classes.root,
        }}
        {...otherProps}
        disabled={pending}
        variant="contained"
      />
      <CircularProgress size={24} className={classes.progress} />
    </div>
  ) : (
    <Button
      classes={{
        root: classes.root,
      }}
      {...otherProps}
      variant="contained"
    />
  ));

  return (<Btn />);
};

NeuButton.propTypes = {
  elevation: PropTypes.number,
  borderRadius: PropTypes.number,
  textColor: PropTypes.any,
  pending: PropTypes.bool,
};

export default NeuButton;
