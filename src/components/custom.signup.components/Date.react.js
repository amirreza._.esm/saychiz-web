import 'date-fns';
import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import NeuTextField from '../neumorphism/NeuTextField.react';

export default function MaterialUIPicker(prop) {
  const [selectedDate, setSelectedDate] = React.useState(null);


  function formatDate(date) {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }
  const handleDateChange = (date) => {
    const formatedDate = formatDate(date);
    setSelectedDate(formatedDate);
    prop.onChange(formatedDate);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardDatePicker
        TextFieldComponent={NeuTextField}
        fullWidth
        margin="dense"
        id="date-picker-dialog"
        label="Date of Birth"
        format="yyyy-MM-dd"
        value={selectedDate}
        onChange={handleDateChange}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
        error={prop.error}
      />
    </MuiPickersUtilsProvider>
  );
}
