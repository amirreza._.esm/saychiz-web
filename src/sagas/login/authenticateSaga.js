import {
  call, put, takeLatest,
} from 'redux-saga/effects';
import * as Api from '@api';
import { setLoginError, authenticate, setLoginPending } from '@actions/loginActionCreator';
import { setAuthToken, setCurrentUser } from '@actions/currentUserActionCreator';
import { setIsLoggedIn, setIsUserVerified } from '@actions/appStateActionCreator';
import { setVerificationCode } from '@actions/verificationActionCreator';

// TODO: handle errors with try catch
function* authenticateWorkerSaga({ payload }) {
  const { usernameEmail, password } = payload;
  yield put(setLoginError(''));
  yield put(setLoginPending(true));
  try {
    const results = yield call(Api.authenticate, usernameEmail, password);
    // const results = { date: { token: '', user: {}, verfiyCode: 0 } };
    const { user, token } = results.data;
    yield put(setAuthToken(token));
    yield put(setCurrentUser(user));
    yield call(Api.storeToken, token);
    yield put(setIsLoggedIn(true));
    if (user.verifyStatus) {
      yield put(setIsUserVerified(true));
    } else {
      const { data: { code: verifyCode } } = yield call(Api.resendVerifyEmail, user.id);
      yield put(setIsUserVerified(false));
      yield put(setVerificationCode(verifyCode));
    }
  } catch (e) {
    if (e.data?.detail) {
      yield put(setLoginError(e.data.detail));
    } else {
      console.log(e);
    }
  } finally {
    yield put(setLoginPending(false));
  }
}

export default function* authenticateSaga() {
  yield takeLatest(authenticate, authenticateWorkerSaga);
}
