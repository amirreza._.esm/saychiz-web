import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  SEARCH_SET_IS_OPEN,
  SEARCH_SET_IS_LOADING,
  SEARCH_SET_NO_RESULTS,
  SEARCH_SET_QUERY,
  SEARCH_SET_RESULTS,
  SEARCH_APPEND_RESULTS,
  SAGA_UPDATE_SEARCH_RESULTS,
  SEARCH_SET_NEXT_PAGE_URL,
  SAGA_SEARCH_LOAD_MORE,
} = ActionTypes;

export const setIsSearchOpen = createAction(SEARCH_SET_IS_OPEN);
export const setIsSearchLoading = createAction(SEARCH_SET_IS_LOADING);
export const setSearchNoResults = createAction(SEARCH_SET_NO_RESULTS);
export const setSearchQuery = createAction(SEARCH_SET_QUERY);
export const setSearchResults = createAction(SEARCH_SET_RESULTS);
export const appendSearchResults = createAction(SEARCH_APPEND_RESULTS);
export const setSearchNextPageUrl = createAction(SEARCH_SET_NEXT_PAGE_URL);
export const updateSearchResults = createAction(SAGA_UPDATE_SEARCH_RESULTS);
export const searchLoadMore = createAction(SAGA_SEARCH_LOAD_MORE);
