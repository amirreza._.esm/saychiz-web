import { createAction } from 'redux-actions';
import { ActionTypes } from '@constants';

const {
  POST_DETAIL_SET_POST,
  POST_DETAIL_SET_COMMENT_LIST,
  POST_DETAIL_SET_LIKERS_LIST,
  POST_DETAIL_IS_POST_LOADING,
  POST_DETAIL_ARE_COMMENTS_LOADING,
  SAGA_GET_POST_COMMENTS,
  SAGA_GET_POST,
  SAGA_GET_POST_LIKER_LIST,
} = ActionTypes;

export const setPost = createAction(POST_DETAIL_SET_POST);
export const setCommentList = createAction(POST_DETAIL_SET_COMMENT_LIST);
export const setLikersList = createAction(POST_DETAIL_SET_LIKERS_LIST);
export const setIsPostLoading = createAction(POST_DETAIL_IS_POST_LOADING);
export const setAreCommentsLoading = createAction(POST_DETAIL_ARE_COMMENTS_LOADING);

export const getPost = createAction(SAGA_GET_POST);
export const getPostComments = createAction(SAGA_GET_POST_COMMENTS);
export const getPostLikersList = createAction(SAGA_GET_POST_LIKER_LIST);
