import {
  put, call, takeLatest, delay,
} from 'redux-saga/effects';
import * as Api from '@api';
import {
  checkEmailAvailable, setEmailAvailable,
} from '@actions/signupActionCreator';
import { stateModes } from '@constants';

export function* validateEmailWorkerSaga({ payload }) {
  yield delay(300);
  yield put(setEmailAvailable(stateModes.PENDING));
  try {
    yield call(Api.validateEmail, payload);
    yield put(setEmailAvailable(stateModes.SUCCESS));
  } catch (e) {
    yield put(setEmailAvailable(stateModes.FAIL));
  }
}

export default function* validateEmailSaga() {
  yield takeLatest(checkEmailAvailable, validateEmailWorkerSaga);
}
