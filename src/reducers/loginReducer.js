import { handleActions } from 'redux-actions';
import { setLoginError, setLoginPending } from '@actions/loginActionCreator';

const initialState = {
  error: '',
  isPending: false,
};

export default handleActions({
  [setLoginError]: (state, { payload }) => ({ ...state, error: payload }),
  [setLoginPending]: (state, { payload }) => ({ ...state, isPending: payload }),
}, initialState);
