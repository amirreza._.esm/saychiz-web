import React from 'react';
import AudioPlayer from './AudioPlayer.react';
import { Box } from '@material-ui/core';
import trendsMock from '../assets/__mocks__/trends.png';
import Search from './search/Search.react';

const ToolBar = () => {
  return (
    <Box
      pt={2}
      height="100vh"
      position="relative"
    >
      <Search />
      <Box
        mt={2}
        css={{ backgroundImage: `url(${trendsMock})`, backgroundSize: 'cover' }}
        width={320}
        height={400}
      />
      <Box
        display="flex"
        justifyContent="center"
      >
        <AudioPlayer />
      </Box>
    </Box>
  );
};

export default ToolBar;
