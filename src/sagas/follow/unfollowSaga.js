import {
  put, call, takeLatest,
} from 'redux-saga/effects';
import * as Api from '../../utils/Api';
import {
  unfollowUser,
} from '@actions/followActionCreator';
import { removeUserFollowing } from '@actions/currentUserActionCreator';

function* worker({ payload }) {
  try {
    yield call(Api.unfollow, payload.visitedUser, payload.currentUser);
    yield put(removeUserFollowing(payload.visitedUser));
  } catch (e) {
    if (e.status === 400) {
      console.log('already followed');
    }
  }
}

export default function* followSaga() {
  yield takeLatest(unfollowUser, worker);
}
