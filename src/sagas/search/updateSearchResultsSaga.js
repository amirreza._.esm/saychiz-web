import {
  takeLatest, delay, put, call,
} from 'redux-saga/effects';
import {
  updateSearchResults, setIsSearchLoading, setSearchResults, setSearchNoResults,
} from '@actions/searchActionCreator';
import * as Api from '@api';

function* updateSearchResultsWorkerSaga({ payload: query }) {
  yield delay(300);
  yield put(setIsSearchLoading(true));
  try {
    if (query) {
      const { data } = yield call(Api.searchByQuery, query);
      yield put(setSearchResults(data.results));
      if (data.results.length === 0) yield put(setSearchNoResults(true));
      else yield put(setSearchNoResults(false));
    } else {
      yield put(setSearchResults([]));
      yield put(setSearchNoResults(false));
    }
  } catch (e) {
    yield put(setSearchResults([]));
  } finally {
    yield put(setIsSearchLoading(false));
  }
}

export default function* updateSearchResultsSaga() {
  yield takeLatest(updateSearchResults, updateSearchResultsWorkerSaga);
}
