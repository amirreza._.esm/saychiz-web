import { all, call } from 'redux-saga/effects';
import authenticateSaga from './login/authenticateSaga';
import loginSaga from './login/loginSaga';
import signOutSaga from './login/signOutSaga';
import signupSaga from './signup/signupSaga';
import updateCountryListSaga from './signup/updateCountryListSaga';
import validateEmailSaga from './signup/validateEmailSaga';
import validateUsernameSaga from './signup/validateUsernameSaga';
import resendVerifyEmailSaga from './signup/resendVerifyEmailSaga';
import verifyUserSaga from './verification/verifyUserSaga';
import createPostSaga from './post/createPostSaga';
import loadTimeLinePostsSaga from './timeline/loadTimeLinePostsSaga';
import getVisitedUserSaga from './profile/getVisitedUserSaga';
import getVisitedUserFollowers from './profile/getVisitedUserFollowersSaga';
import getVisitedUserFollowing from './profile/getVisitedUserFollowingSaga';
import getNextFollowingSaga from './profile/getNextUserFollowingSaga';
import getNextFollowersSaga from './profile/getNextUserFollowersSaga';
import getCurrentUserFollowersSaga from './profile/getCurrentUserFollowersSaga';
import getCurrentUserFollowingSaga from './profile/getCurrentUserFollowingSaga';
import likePostSaga from './post/likePostSaga';
import unlikePostSaga from './post/unlikePostSaga';
import deletePostSaga from './post/deletePostSaga';
import createCommentSaga from './post/createCommentSaga';
import getPostCommentsSaga from './post/getPostCommentsSaga';
import getPostDetailMainPostSaga from './post/getPostDetailMainPostSaga';
import getPostLikerListSaga from './post/getPostLikerListSaga';
import updateSearchResultsSaga from './search/updateSearchResultsSaga';
import timelineLoadMoreSaga from './timeline/timelineLoadMoreSaga';
import searchLoadMoreSaga from './search/searchLoadMoreSaga';
import followUser from './follow/followSaga';
import unfollowUser from './follow/unfollowSaga';
import loadProfilePostsSaga from './profile/loadProfilePostsSaga';
import profilePostsLoadMoreSaga from './profile/profilePostsLoadMoreSaga';


export default function* () {
  yield all([
    call(authenticateSaga),
    call(loginSaga),
    call(signOutSaga),
    call(signupSaga),
    call(updateCountryListSaga),
    call(validateEmailSaga),
    call(validateUsernameSaga),
    call(resendVerifyEmailSaga),
    call(verifyUserSaga),
    call(createPostSaga),
    call(getVisitedUserSaga),
    call(getVisitedUserFollowers),
    call(getVisitedUserFollowing),
    call(getNextFollowingSaga),
    call(getNextFollowersSaga),
    call(getCurrentUserFollowersSaga),
    call(getCurrentUserFollowingSaga),
    call(createCommentSaga),
    call(deletePostSaga),
    call(loadTimeLinePostsSaga),
    call(likePostSaga),
    call(unlikePostSaga),
    call(getPostCommentsSaga),
    call(getPostDetailMainPostSaga),
    call(getPostLikerListSaga),
    call(updateSearchResultsSaga),
    call(timelineLoadMoreSaga),
    call(searchLoadMoreSaga),
    call(followUser),
    call(unfollowUser),
    call(loadProfilePostsSaga),
    call(profilePostsLoadMoreSaga),
  ]);
}
