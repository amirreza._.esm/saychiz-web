import React, { useRef, useEffect } from 'react';
import { useTheme } from '@material-ui/core';

const getNormalizedDataArray = (analyser) => {
  if (analyser) {
    const dataArray = new Float32Array(analyser.fftSize);
    analyser.getFloatTimeDomainData(dataArray);

    // const max = Math.max(...dataArray);
    // return dataArray.map((n) => (n * 1.0) / (max * 1.0));
    return dataArray;
  }
  return null;
};

// const getFrqDataArray = (analyser) => {
//   if (analyser) {
//     const dataArray = new Float32Array(analyser.fftSize);
//     analyser.getFloatFrequencyData(dataArray);
//     const maxDb = analyser.maxDecibels;
//     const minDb = analyser.minDecibels;
//     const freqScale = 1 / (maxDb - minDb);
//     const freqOffset = minDb;
//     return dataArray.map((n) => freqScale * (n - freqOffset));
//   }
//   return null;
// };

const RecordVisualizer = ({ analyser, isRecording }) => {
  const theme = useTheme();
  const canvasRef = useRef();
  const animationFrameRef = useRef();
  // console.log(isRecording);

  const drawOnCanvas = (dataArray, render) => {
    if (canvasRef.current) {
      const ctx = canvasRef.current.getContext('2d');
      if (!render) {
        const { width: w, height: h } = canvasRef.current.getBoundingClientRect();
        const baseLineY = h / 2;
        ctx.strokeStyle = theme.palette.secondary.main;
        ctx.clearRect(0, 0, w, h);
        ctx.beginPath();
        ctx.moveTo(0, baseLineY);
        ctx.lineTo(w, baseLineY);
        ctx.stroke();
        ctx.closePath();
        // console.log('not drawing');

        return;
      }
      const points = dataArray.length;

      const { width: w, height: h } = canvasRef.current.getBoundingClientRect();
      const density = 3;
      const baseLineY = h / 2;
      const segWidth = w / (points / 1);
      const cornerNerfRange = 100;
      const signalBuffer = 1;

      ctx.clearRect(0, 0, w, h);
      ctx.beginPath();
      ctx.moveTo(0, baseLineY);
      ctx.strokeStyle = theme.palette.secondary.main;

      for (let i = 0; i < points; i += density) {
        let yFactor = 0;
        if (i < cornerNerfRange) {
          yFactor = dataArray[i] * (i / cornerNerfRange);
        } else if (i >= points - cornerNerfRange) {
          yFactor = dataArray[i] * ((points - i) / cornerNerfRange);
        } else {
          yFactor = dataArray[i];
        }
        yFactor *= signalBuffer;

        const x = i * segWidth;
        const y = baseLineY + (yFactor * (h / 2));
        ctx.lineTo(x, y);
      }
      ctx.stroke();
      ctx.closePath();
    }
  };

  const animationUpdate = () => {
    const array = getNormalizedDataArray(analyser);
    if (array) {
      drawOnCanvas(array, isRecording);
    }
    requestAnimationFrame(animationUpdate);
  };

  useEffect(() => {
    animationFrameRef.current = requestAnimationFrame(animationUpdate);
    return () => {
      cancelAnimationFrame(animationFrameRef.current);
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [analyser, isRecording]);

  return (
    <canvas
      ref={canvasRef}
      width={400}
      height={40}
    />
  );
};

export default RecordVisualizer;
