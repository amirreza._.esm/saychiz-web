/* eslint-disable max-len */
import { handleActions } from 'redux-actions';
import {
  setVisitedUser, setVisitedUserFollowers, setVisitedUserFollowing,
  setVisitedUserError, setNextFollowingPage, setNextFollowersPage,
  addVisitedUserFollowers, addVisitedUserFollowing,
} from '@actions/visitedUserActionCreator';

const initialState = {
  visitedUser: {
    id: '6',
    username: '78m.ali',
    email: '78eslami@gmail.com',
    nickname: 'Sky',
    birthday: '1999-04-05',
    gender: '1',
    isPrivate: 'false',
    avatar: 'https://www.meme-arsenal.com/memes/3cbd5a9bfb11fd86f4106df9f3493a54.jpg',
    about: 'gamer',
    countFollower: '230',
    countFollowing: '350',
    countPost: '25',
    verifyStatus: 'true',
    country: 'Iran',
    phone: '+11111222',
    theme: '',
  },
  visitedUserFollowers: [
    {
      username: 'farinamHZ',
      nickname: 'fari',
      avatar: '',
      about: 'You only get one shot, do not miss your chance to blow!',
    },
    {
      username: 'blackou7',
      nickname: 'davane',
      avatar: 'https://carbonmade-media.accelerator.net/25183117;640x640.png?auto=webp',
      about: 'Remmember the game',
    },
    {
      username: 'Divergent',
      nickname: 'Luna',
      avatar: 'https://i1.sndcdn.com/avatars-000624742044-q2iwow-t500x500.jpg',
      about: 'Abandon All Hopes',
    },
  ],
  visitedUserFollowing: [{
    username: 'zolfa',
    nickname: 'zolfash',
    avatar: 'https://img.lovepik.com/element/40143/1122.png_860.png',
    about: 'kachal kachal kalache roghane kale pache.',
  }, {
    username: 'Divergent',
    nickname: 'Luna',
    avatar: 'https://i1.sndcdn.com/avatars-000624742044-q2iwow-t500x500.jpg',
    about: 'Abandon All Hopes',
  },
  {
    username: 'farinamHZ',
    nickname: 'fari',
    avatar: '',
    about: 'You only get one shot, do not miss your chance to blow!',
  },
  ],
  visitedUserError: false,
  nextFollowingPage: null,
  nextFollowersPage: null,
};

export default handleActions({
  [setVisitedUser]: (state, { payload }) => ({ ...state, visitedUser: payload }),
  [setVisitedUserFollowers]: (state, { payload }) => ({ ...state, visitedUserFollowers: payload }),
  [setVisitedUserFollowing]: (state, { payload }) => ({ ...state, visitedUserFollowing: payload }),
  [setVisitedUserError]: (state, { payload }) => ({ ...state, visitedUserError: payload }),
  [setNextFollowingPage]: (state, { payload }) => ({ ...state, nextFollowingPage: payload }),
  [setNextFollowersPage]: (state, { payload }) => ({ ...state, nextFollowersPage: payload }),
  [addVisitedUserFollowers]: (state, { payload }) => ({ ...state, visitedUserFollowers: state.visitedUserFollowers.concat(payload) }),
  [addVisitedUserFollowing]: (state, { payload }) => ({ ...state, visitedUserFollowing: state.visitedUserFollowing.concat(payload) }),
}, initialState);
