
import { takeLatest, put, call } from 'redux-saga/effects';
import { setIsPostLoading, setPost, getPost } from '@actions/postDetailActionCreator';
import * as Api from '@api';

function* worker({ payload }) {
  const { postId } = payload;
  yield put(setIsPostLoading(true));
  try {
    const { data } = yield call(Api.getPostById, postId);
    yield put(setPost(data));
  } catch (e) {
    if (e.status === 404) {
      yield put(setPost(null));
    }
  } finally {
    yield put(setIsPostLoading(false));
  }
}

export default function* getPostDetailMainPostSaga() {
  yield takeLatest(getPost, worker);
}
