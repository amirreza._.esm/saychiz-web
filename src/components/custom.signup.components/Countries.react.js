import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import NeuTextField from '../neumorphism/NeuTextField.react';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(() => ({
  formControl: {
    minWidth: 120,
    width: '100%',
  },
}));

export default function CountrySelect(prop) {
  const classes = useStyles();
  const [country, setCountry] = React.useState('');

  const handleChange = (event) => {
    setCountry(event.target.value);
    prop.callbackFromParent(event.target.value);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">{prop.lid}</InputLabel>
        <NeuTextField
          select
          label="Country"
          value={country}
          margin="dense"
          fullWidth
          onChange={handleChange}
          required
          error={prop.error}
        >
          {
            prop.countryList.length > 0
              ? prop.countryList.map((countryObj) => (
                <MenuItem key={countryObj.country_name} value={countryObj.country_name}>
                  {`${countryObj.country_name}: ${countryObj.code}`}
                </MenuItem>
              ))
              : (
                <MenuItem>
                  <CircularProgress
                    disableShrink
                    size="20px"
                  />
                </MenuItem>
              )
          }
        </NeuTextField>
      </FormControl>
    </div>
  );
}
