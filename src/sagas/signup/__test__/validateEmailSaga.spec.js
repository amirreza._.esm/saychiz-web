import { testSaga } from 'redux-saga-test-plan';
import { validateEmailWorkerSaga } from '../validateEmailSaga';
import { setEmailAvailable, checkEmailAvailable } from '@actions/signupActionCreator';
import { stateModes } from '@constants';

describe('validate email saga', () => {
  it('should set emailAvailable true if email is available', () => {
    const email = 'someEmail';

    const saga = testSaga(validateEmailWorkerSaga, checkEmailAvailable(email));
    saga.next();
    saga.next().put(setEmailAvailable(stateModes.PENDING));
    saga.next();
    saga.next().put(setEmailAvailable(stateModes.SUCCESS));
    saga.next().isDone();
  });
  it("should set emailAvailable false if email isn't available", () => {
    const email = 'someOtherEmail';

    const saga = testSaga(validateEmailWorkerSaga, checkEmailAvailable(email));

    saga.next();
    saga.next().put(setEmailAvailable(stateModes.PENDING));
    saga.next();
    saga.throw({}).put(setEmailAvailable(stateModes.FAIL));
    saga.next().isDone();
  });
});
