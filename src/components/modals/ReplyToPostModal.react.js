import React, {
  useState, useRef, useEffect, useCallback,
} from 'react';
import * as PropTypes from 'prop-types';
import Surface from '../common/Surface.react';
import {
  Grid, Box, Typography, makeStyles, IconButton, Badge,
} from '@material-ui/core';
import NeuButton from '../neumorphism/NeuButton.react';
import NeuIconButton from '../neumorphism/NeuIconButton.react';
import NeuTextField from '../neumorphism/NeuTextField.react';
import { backgroundGradient } from '../../styles/common/gradient';

import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';
import SendIcon from '@material-ui/icons/Send';
import MicIcon from '@material-ui/icons/Mic';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import StopIcon from '@material-ui/icons/Stop';
import DeleteIcon from '@material-ui/icons/Delete';
import AttachFileIcon from '@material-ui/icons/AttachFile';
import { voiceSetting } from '@constants';
import { useSelector, useDispatch } from 'react-redux';
import RecordVisualizer from '../postComponents/RecordVisualizer.react';
import NeuDialog from '../neumorphism/NeuDialog.react';
import { closeReplyModal, createComment } from '@actions/ReplyToPostActionCreator';
import getBlobDuration from 'get-blob-duration';

const useStyles = makeStyles((theme) => ({
  backgroundGradient: backgroundGradient(theme),
  headerSection: {
    marginBottom: theme.spacing(2),
    display: 'flex',
  },
  higherSection: {
    marginBottom: theme.spacing(2),
    display: 'flex',
    flexBasis: '60%',
  },
  lowerSection: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexBasis: '40%',
  },
  avatar: {
    width: theme.spacing(8),
    height: theme.spacing(8),
  },
  voiceLineWrapper: {
    height: theme.spacing(6),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textWrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  voiceDeckWrapper: {
    flexBasis: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  voiceControlWrapper: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  TLBtn: {
    width: theme.spacing(5.5),
    height: theme.spacing(4),
    borderTopRightRadius: 5,
    // borderBottomRightRadius: 5,
    // borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  TRBtn: {
    width: theme.spacing(5.5),
    height: theme.spacing(4),
    // borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    borderTopLeftRadius: 5,
    // borderBottomLeftRadius: 5,
  },
  BLBtn: {
    width: theme.spacing(5.5),
    height: theme.spacing(4),
    // borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    borderTopLeftRadius: 5,
    // borderBottomLeftRadius: 5,
  },
  BRBtn: {
    width: theme.spacing(5.5),
    height: theme.spacing(4),
    borderTopRightRadius: 5,
    // borderBottomRightRadius: 5,
    // borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  sendBtn: {
    height: theme.spacing(5),
  },
}));

const ReplyToPostModalBody = ({ handleClose }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [caption, setCaption] = useState('');
  const [isRecording, setIsRecording] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [voiceDataBlobArray, setVoiceDataBlobArray] = useState([]);
  const [analyser, setAnalyser] = useState(null);
  const [audioType, setAudioType] = useState(null); // 'voice', 'file
  const [audioDuration, setAudioDuration] = useState(0);
  const [isFileSizeToBig, setIsFileSizeToBig] = useState(false);

  const voiceURLRef = useRef();
  const attachedFileRef = useRef();

  // components
  const mediaRecorderRef = useRef();
  const voicePlayerRef = useRef();
  const audioInputRef = useRef();

  // settings
  const recorderTimeSlice = 500;
  const { MAXIMUM_VOICE_LENGTH_MS, MAXIMUM_AUDIO_FILE_BYTES } = voiceSetting;


  const currentUser = useSelector((state) => state.currentUser.currentUser);
  const sourcePostId = useSelector((state) => state.replyToPost.sourcePostId);
  const sourceAuthor = useSelector((state) => state.replyToPost.sourceAuthor);


  const disableAudioControls = !audioType;
  const disableCreatePost = isFileSizeToBig;

  const resetRecorder = () => {
    const mediaRecorder = mediaRecorderRef.current;
    if (mediaRecorder) {
      setIsRecording(false);
      const { state } = mediaRecorder;
      if (state !== 'inactive') {
        mediaRecorder.stop();
        setTimeout(() => {
          setVoiceDataBlobArray([]);
        }, 100);
      } else {
        setVoiceDataBlobArray([]);
      }
    }
  };

  const resetVoicePlayer = () => {
    setIsPlaying(false);
    const voicePlayer = voicePlayerRef.current;
    window.URL.revokeObjectURL(voiceURLRef.current);
    voiceURLRef.current = null;
    if (voicePlayer) {
      voicePlayer.pause();
      voicePlayer.src = '';
      voicePlayer.currentTime = 0;
    }
  };

  const resetAttachedFile = () => {
    attachedFileRef.current = null;
    setIsFileSizeToBig(false);
    setAudioDuration(0);
  };

  const resetEverything = () => {
    setCaption('');
    setAudioType(null);

    resetRecorder();
    resetVoicePlayer();
    resetAttachedFile();
  };

  const setupVoicePlayer = (blobOrFile) => {
    const voicePlayer = voicePlayerRef.current;
    window.URL.revokeObjectURL(voiceURLRef.current);
    if (voicePlayer) {
      const blobURL = window.URL.createObjectURL(blobOrFile);
      voiceURLRef.current = blobURL;
      voicePlayer.src = blobURL;
      voicePlayer.load();
    }
  };

  const handleFileInputChange = (e) => {
    const voicePlayer = voicePlayerRef.current;
    const file = e.target.files[0];
    if (file) {
      attachedFileRef.current = file;

      if (file.size > MAXIMUM_AUDIO_FILE_BYTES) {
        setIsFileSizeToBig(true);
      }

      resetRecorder();
      resetVoicePlayer();

      voicePlayer.onloadedmetadata = () => {
        setAudioDuration(voicePlayer.duration);
      };
      setupVoicePlayer(file);

      setAudioType('file');
    }
  };

  const handleToggleRecord = useCallback(() => {
    const mediaRecorder = mediaRecorderRef.current;
    if (mediaRecorder) {
      const { state } = mediaRecorder;
      switch (state) {
        case 'inactive':
          resetAttachedFile();
          setAudioType('voice');
          mediaRecorderRef.current.start(recorderTimeSlice);
          break;
        case 'recording':
          mediaRecorderRef.current.pause();
          break;
        case 'paused':
          mediaRecorderRef.current.resume();

          break;
        default:
          break;
      }
      setIsRecording(!isRecording);
    }
  }, [isRecording]);

  const handleTogglePlay = () => {
    const voicePlayer = voicePlayerRef.current;
    if (voicePlayer) {
      if (isPlaying) {
        voicePlayer.pause();
      } else {
        if (audioType === 'voice') {
          const voiceBlob = new Blob(voiceDataBlobArray);
          setupVoicePlayer(voiceBlob);
          voicePlayer.play();
        }
        if (audioType === 'file') {
          voicePlayer.play();
        }
      }
      setIsPlaying(!isPlaying);
    }
  };


  const handleDeleteVoice = () => {
    resetEverything();
  };

  const handleCaptionChange = (e) => {
    setCaption(e.target.value.substring(0, 300));
  };

  const handleSubmit = () => {
    const mediaRecorder = mediaRecorderRef.current;
    const attachedFile = attachedFileRef.current;
    if (audioType === 'file') {
      if (attachedFile) {
        const fileFormat = attachedFile.name.substring(attachedFile.name.lastIndexOf('.'));
        const authorId = currentUser.id;
        const fileType = 2;
        dispatch(createComment({
          sourcePostId,
          authorId,
          caption: caption.trim(),
          file: attachedFile,
          fileName: `${authorId}-${new Date().getTime()}${fileFormat}`,
          fileType,
          duration: audioDuration,
        }));
      }
      resetEverything();
    }
    if (audioType === 'voice') {
      if (mediaRecorder) {
        if (mediaRecorder.state !== 'inactive') {
          mediaRecorder.stop();
        }
        setTimeout(() => {
          const voiceBlob = new Blob(voiceDataBlobArray, { type: 'audio/ogg' });
          if (voiceBlob.size > 0) {
            getBlobDuration(voiceBlob).then((duration) => {
              const authorId = currentUser.id;
              const fileType = 1;
              dispatch(createComment({
                sourcePostId,
                authorId,
                caption: caption.trim(),
                file: voiceBlob,
                fileName: `${authorId}-${new Date().getTime()}.ogg`,
                fileType,
                duration,
              }));
            });
          }
          resetEverything();
        }, 100);
      }
    }
    handleClose();
  };

  useEffect(() => {
    if (voiceDataBlobArray.length > (MAXIMUM_VOICE_LENGTH_MS / recorderTimeSlice) && isRecording) {
      handleToggleRecord();
    }
  }, [MAXIMUM_VOICE_LENGTH_MS, handleToggleRecord, isRecording, voiceDataBlobArray.length]);

  useEffect(() => {
    const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    navigator.mediaDevices.getUserMedia({ audio: true }).then(
      (stream) => {
        const mediaRecorder = new MediaRecorder(stream);
        mediaRecorderRef.current = mediaRecorder;
        mediaRecorder.addEventListener('dataavailable', (event) => {
          setVoiceDataBlobArray((array) => [...array, event.data]);
        });
        const source = audioCtx.createMediaStreamSource(stream);
        const audioAnalyser = audioCtx.createAnalyser();
        source.connect(audioAnalyser);
        setAnalyser(audioAnalyser);
        audioAnalyser.fftSize = 2048;
        // analyser.smoothingTimeConstant = 0.6;
        // source.connect(audioCtx.destination);
      },
    );

    voicePlayerRef.current.addEventListener('ended', () => {
      setIsPlaying((state) => !state);
    });

    return () => {

    };
  }, []);


  return (
    <Box
      pt={1}
      pr={1}
      pb={2}
      pl={3}
      flexDirection="column"
      clone
    >
      <Surface borderRadius={3} elevation={2} className={classes.backgroundGradient}>
        {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
        <audio ref={voicePlayerRef} controls={false} />
        <input
          ref={audioInputRef}
          type="file"
          accept="audio/mp3, audio/mpeg, audio/mp4, audio/basic, audio/x-midi,
                    audio/webm, audio/vorbis, audio/x-pn-realaudio, audio/vnd.rn-realaudio,
                              audio/x-pn-realaudio, audio/vnd.rn-realaudio, audio/wav, audio/x-wav,
                                        audio/ogg"
          style={{ visibility: 'hidden', width: 0, height: 0 }}
          onChange={handleFileInputChange}
        />
        <div className={classes.headerSection}>
          <Grid container>
            <Grid item xs container alignItems="center">
              <Typography>{`Reply to @${sourceAuthor?.username}`}</Typography>
            </Grid>
            <Grid item>
              <IconButton onClick={handleClose}>
                <CloseOutlinedIcon />
              </IconButton>
            </Grid>
          </Grid>
        </div>
        <div className={classes.higherSection}>
          <Grid container spacing={2}>
            <Grid item xs container justify="center" alignItems="center">
              <div className={classes.voiceDeckWrapper}>
                <Surface borderRadius={1} className={classes.voiceLineWrapper}>
                  <RecordVisualizer analyser={analyser} isRecording={isRecording} />
                </Surface>
                <div className={classes.voiceControlWrapper} />
              </div>
            </Grid>
            <Grid item xs={3} container justify="space-around" alignItems="center" wrap="wrap" spacing={1}>
              <Grid item xs={6} container justify="flex-end" alignItems="flex-end">
                <NeuIconButton
                  className={classes.TLBtn}
                  onClick={handleToggleRecord}
                  textColor={(() => {
                    if (isRecording) return 'error';
                    if (audioType === 'voice') return 'primary';
                    return 'secondary';
                  })()}
                  elevation={isRecording ? -0.5 : 0.5}
                >
                  <MicIcon />
                </NeuIconButton>
              </Grid>
              <Grid item xs={6} container justify="flex-start" alignItems="flex-end">
                <NeuIconButton
                  className={classes.TRBtn}
                  textColor={audioType === 'file' ? 'primary' : 'secondary'}
                  elevation={0.5}
                  onClick={() => {
                    audioInputRef.current.click();
                  }}
                >
                  <Badge color="error" variant="dot" invisible={!disableCreatePost}>
                    <AttachFileIcon />
                  </Badge>
                </NeuIconButton>
              </Grid>
              <Grid item xs={6} container justify="flex-end" alignItems="flex-start">
                <NeuIconButton
                  className={classes.BLBtn}
                  onClick={handleTogglePlay}
                  textColor={!disableAudioControls ? 'secondary' : (theme) => theme.palette.action.disabled}
                  elevation={0.5}
                  disabled={disableAudioControls}
                >
                  {(() => {
                    if (isPlaying) {
                      if (audioType === 'voice') return <StopIcon />;
                      if (audioType === 'file') return <PauseIcon />;
                    }
                    return <PlayArrowIcon />;
                  })()}
                </NeuIconButton>
              </Grid>
              <Grid item xs={6} container justify="flex-start" alignItems="flex-start">
                <NeuIconButton
                  className={classes.BRBtn}
                  onClick={handleDeleteVoice}
                  textColor={!disableAudioControls ? 'error' : (theme) => theme.palette.action.disabled}
                  elevation={0.5}
                  disabled={disableAudioControls}
                >
                  <DeleteIcon />
                </NeuIconButton>
              </Grid>
            </Grid>
          </Grid>
        </div>
        <div className={classes.lowerSection}>
          <Grid container spacing={2}>
            <Grid item xs container justify="center" alignItems="center">
              <NeuTextField
                fullWidth
                borderRadius={1}
                multiline
                label="add caption to this comment..."
                value={caption}
                onChange={handleCaptionChange}
              />
            </Grid>
            <Grid item xs={3} container justify="center" alignItems="center" spacing={1}>
              <NeuButton
                borderRadius={5}
                className={classes.sendBtn}
                textColor={!disableCreatePost ? 'primary' : (theme) => theme.palette.action.disabled}
                disabled={disableCreatePost}
                onClick={handleSubmit}
              >
                post
                &nbsp;
                <SendIcon style={{ fontSize: '1.25rem' }} />
              </NeuButton>
            </Grid>
          </Grid>
        </div>
      </Surface>
    </Box>
  );
};

ReplyToPostModalBody.propTypes = {
  handleClose: PropTypes.func,
};

const ReplyToPostModal = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.replyToPost.isOpen);
  const handleClose = () => {
    dispatch(closeReplyModal());
  };

  return (
    <NeuDialog
      open={isOpen}
      onClose={handleClose}
      borderRadius={5}
      maxWidth="sm"
      fullWidth
    >
      <ReplyToPostModalBody
        handleClose={handleClose}
      />
    </NeuDialog>
  );
};

export default ReplyToPostModal;
