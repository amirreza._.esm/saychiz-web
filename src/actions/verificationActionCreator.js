import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  VERIFICATION_MODAL_OPEN,
  VERIFICATION_MODAL_CLOSE,
  VERIFICATION_SET_CODE,
  VERIFICATION_SET_ERROR,
  VERIFICATION_SET_PENDING,
  SAGA_VERIFY_USER,
} = ActionTypes;


export const openVerificationModal = createAction(VERIFICATION_MODAL_OPEN);
export const closeVerificationModal = createAction(VERIFICATION_MODAL_CLOSE);


export const setVerificationCode = createAction(VERIFICATION_SET_CODE);
export const setVerificationError = createAction(VERIFICATION_SET_ERROR);
export const setVerificationPending = createAction(VERIFICATION_SET_PENDING);
export const verifyUser = createAction(SAGA_VERIFY_USER);
