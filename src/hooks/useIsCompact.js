import { useMediaQuery } from '@material-ui/core';

const useIsCompact = () => {
  const isCompact = useMediaQuery((theme) => theme.breakpoints.down('xs'));
  return isCompact;
};

export default useIsCompact;
