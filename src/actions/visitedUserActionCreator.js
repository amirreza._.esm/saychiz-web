import { ActionTypes } from '../constants/AppConstants';
import { createAction } from 'redux-actions';

const {
  VISITED_USER_SET_VISITED_USER,
  VISITED_USER_SET_VISITED_USER_FOLLOWERS,
  VISITED_USER_SET_VISITED_USER_FOLLOWING,
  VISITED_USER_SET_VISITED_USER_ERROR,
  VISITED_USER_SET_NEXT_FOLLOWING_PAGE,
  VISITED_USER_SET_NEXT_FOLLOWERS_PAGE,
  VISITED_USER_ADD_VISITED_USER_FOLLOWERS,
  VISITED_USER_ADD_VISITED_USER_FOLLOWING,
  SAGA_VISITED_USER,
  SAGA_VISITED_USER_GET_FOLLOWERS,
  SAGA_VISITED_USER_GET_FOLLOWING,
  SAGA_VISITED_USER_GET_NEXT_FOLLOWERS,
  SAGA_VISITED_USER_GET_NEXT_FOLLOWING,
} = ActionTypes;

export const setVisitedUser = createAction(VISITED_USER_SET_VISITED_USER);
export const setVisitedUserFollowers = createAction(VISITED_USER_SET_VISITED_USER_FOLLOWERS);
export const setVisitedUserFollowing = createAction(VISITED_USER_SET_VISITED_USER_FOLLOWING);
export const setVisitedUserError = createAction(VISITED_USER_SET_VISITED_USER_ERROR);
export const setNextFollowingPage = createAction(VISITED_USER_SET_NEXT_FOLLOWING_PAGE);
export const setNextFollowersPage = createAction(VISITED_USER_SET_NEXT_FOLLOWERS_PAGE);
export const addVisitedUserFollowers = createAction(VISITED_USER_ADD_VISITED_USER_FOLLOWERS);
export const addVisitedUserFollowing = createAction(VISITED_USER_ADD_VISITED_USER_FOLLOWING);

export const visitedUserData = createAction(SAGA_VISITED_USER);
export const getVisitedUserFollowers = createAction(SAGA_VISITED_USER_GET_FOLLOWERS);
export const getVisitedUserFollowing = createAction(SAGA_VISITED_USER_GET_FOLLOWING);
export const getNextVisitedUserFollowers = createAction(SAGA_VISITED_USER_GET_NEXT_FOLLOWERS);
export const getNextVisitedUserFollowing = createAction(SAGA_VISITED_USER_GET_NEXT_FOLLOWING);
