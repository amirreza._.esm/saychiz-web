import React from 'react';
import {
  Grid, Box, Typography, IconButton, DialogTitle, Divider, DialogContent,
} from '@material-ui/core';

import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';
import { useSelector, useDispatch } from 'react-redux';
import NeuDialog from '../neumorphism/NeuDialog.react';
import { closeLikerListModal } from '@actions/likerListModalActionCreator';
import UserListItem from '../common/UserListItem.react';

const LikerListModalBody = () => {
  const likerList = useSelector((state) => state.likerListModal.likerList);

  return (
    <Box
      maxHeight={500}
    >
      {
        likerList.map((likerUser, index) => (
          <>
            <Box
              my={1}
            >
              <UserListItem user={likerUser} />
            </Box>
            {index !== likerList.length - 1 && <Divider variant="middle" />}
          </>
        ))
      }
    </Box>
  );
};


const LikerListModal = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.likerListModal.isOpen);
  const handleClose = () => {
    dispatch(closeLikerListModal());
  };

  return (
    <NeuDialog
      open={isOpen}
      onClose={handleClose}
      borderRadius={5}
      maxWidth="xs"
      fullWidth
      opacityLevel="dark"
      scroll="paper"
    >
      <DialogTitle>
        <Grid container>
          <Grid item xs container alignItems="center">
            <Typography>User&apos;s who liked this post</Typography>
          </Grid>
          <Grid item>
            <IconButton onClick={handleClose}>
              <CloseOutlinedIcon />
            </IconButton>
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent>
        <LikerListModalBody
          handleClose={handleClose}
        />
      </DialogContent>
    </NeuDialog>
  );
};

export default LikerListModal;
