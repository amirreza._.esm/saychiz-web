import React, { useState, useEffect, useRef } from 'react';
import {
  Box, InputAdornment, IconButton, makeStyles,
  ClickAwayListener, Divider, CircularProgress, fade, Typography,
} from '@material-ui/core';
import NeuTextField from '../neumorphism/NeuTextField.react';
import UserListItem from '../common/UserListItem.react';
import { useDispatch, useSelector } from 'react-redux';
import {
  updateSearchResults, setIsSearchOpen, setSearchQuery, searchLoadMore,
} from '@actions/searchActionCreator';
import { AnimatePresence, motion } from 'framer-motion';
import CancelIcon from '@material-ui/icons/Cancel';
import { isCloseToMaxScroll } from '@utils/scrollUtils';


const useStyles = makeStyles((theme) => ({
  resultsWrapper: {
    backdropFilter: 'blur(20px)',
    backgroundColor: fade(theme.palette.background.paper, 0.5),
    borderColor: theme.palette.background.default,
    borderWidth: 0.5,
    borderStyle: 'solid',
  },
}));

const resultsBoxVariants = {
  mount: {
    opacity: 1,
    scaleY: 1,
    y: 0,
  },
  unmount: {
    opacity: 0,
    scaleY: 0,
    y: -50,
  },
};

const itemVariants = {
  normal: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
  },
  hover: {
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
  },
};

const Search = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const scrollContainerRef = useRef();
  const [isTextFieldFocus, setIsTextFieldFocus] = useState(false);

  const isOpen = useSelector((state) => state.search.isOpen);
  const isLoading = useSelector((state) => state.search.isLoading);
  const noResults = useSelector((state) => state.search.noResults);
  const query = useSelector((state) => state.search.searchQuery);
  const results = useSelector((state) => state.search.searchResults);
  // const { results } = searchMocks;

  const handleScrollChange = () => {
    const scrollContainer = scrollContainerRef.current;
    if (scrollContainer) {
      if (isCloseToMaxScroll(scrollContainer, 25)) {
        dispatch(searchLoadMore());
      }
    }
  };

  useEffect(() => {
    dispatch(updateSearchResults(query));
  }, [dispatch, query]);

  useEffect(() => {
    if (query && isTextFieldFocus) {
      dispatch(setIsSearchOpen(true));
    } else {
      dispatch(setIsSearchOpen(false));
    }
  }, [dispatch, isTextFieldFocus, query, isOpen]);

  const renderResults = () => {
    if (isLoading) {
      return (
        <Box display="flex" justifyContent="center" alignItems="center" p={1}>
          <CircularProgress />
        </Box>
      );
    }
    if (noResults) {
      return (
        <Box display="flex" justifyContent="center" alignItems="center" p={1}>
          <Typography variant="body1" color="textPrimary">{`No results for ${query}`}</Typography>
        </Box>
      );
    }
    return (
      results.map((user, index) => (
        <>
          <Box
            component={motion.div}
            variants={itemVariants}
            animate="normal"
            whileHover="hover"
            p={1}
            onClick={() => dispatch(setSearchQuery(''))}
          >
            <UserListItem user={user} />
          </Box>
          {index < results.length - 1 && <Divider /> }
        </>
      ))
    );
  };

  return (
    <ClickAwayListener onClickAway={() => setIsTextFieldFocus(false)}>
      <div>
        <NeuTextField
          label="Search everywhere"
          value={query}
          onChange={(e) => dispatch(setSearchQuery(e.target.value))}
          onFocus={() => setIsTextFieldFocus(true)}
          fullWidth
          {...(query && {
            InputProps:
          {
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  size="small"
                  onClick={() => dispatch(setSearchQuery(''))}
                >
                  <CancelIcon />
                </IconButton>
              </InputAdornment>
            ),
          },
          })}
        />
        <Box
          position="relative"
        >
          <AnimatePresence>
            { isOpen && results
          && (
          <Box
            borderRadius={25}
            component={motion.div}
            position="absolute"
            maxHeight={400}
            width={1}
            top={16}
            overflow="auto"
            variants={resultsBoxVariants}
            initial="unmount"
            animate="mount"
            exit="unmount"
            transition={{ duration: 0.1, type: 'tween' }}
            ref={scrollContainerRef}
            onScroll={handleScrollChange}
          >
            <div className={classes.resultsWrapper}>
              {renderResults()}
            </div>
          </Box>
          )}
          </AnimatePresence>
        </Box>
      </div>
    </ClickAwayListener>
  );
};

export default Search;
