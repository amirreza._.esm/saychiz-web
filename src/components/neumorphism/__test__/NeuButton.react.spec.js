import React from 'react';

import { render, fireEvent } from '@testing-library/react';
import NeuButton from '../NeuButton.react';

describe('NeuButton', () => {
  it('should render', () => {
    const buttonText = 'text';
    const { queryByText } = render(
      <NeuButton>{buttonText}</NeuButton>,
    );
    expect(queryByText(buttonText)).toBeInTheDocument();
  });
  it('should be clickable', () => {
    const buttonText = 'text';
    const fn = jest.fn();

    const { queryByTestId } = render(
      <NeuButton
        data-testid="btntestid"
        onClick={fn}
      >
        {buttonText}
      </NeuButton>,
    );
    const btn = queryByTestId('btntestid');

    fireEvent.click(btn);

    expect(fn).toBeCalled();
  });
});
