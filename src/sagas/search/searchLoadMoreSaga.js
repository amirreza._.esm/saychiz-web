import {
  call, select, put, takeLeading,
} from 'redux-saga/effects';
import { searchLoadMore, appendSearchResults, setSearchNextPageUrl } from '@actions/searchActionCreator';
import * as Api from '@api';

function* searchLoadMoreWorkerSaga() {
  const nextPageUrl = yield select((state) => state.search.nextPageUrl);
  try {
    if (nextPageUrl) {
      const {
        data: {
        // eslint-disable-next-line no-unused-vars
          count, next, previous, results,
        },
      } = yield call(Api.searchLoadMore, nextPageUrl);

      yield put(appendSearchResults(results));
      yield put(setSearchNextPageUrl(next));
    }
  } catch (e) {
    console.log(e);
  }
}

export default function* searchLoadMoreSaga() {
  yield takeLeading(searchLoadMore, searchLoadMoreWorkerSaga);
}
