import React from 'react';
import {
  Box, Typography, makeStyles, MenuItem, Menu as MuiMenu, Grid, Avatar,
} from '@material-ui/core';
import NeuButton from './neumorphism/NeuButton.react';
import { useSelector, useDispatch } from 'react-redux';
import { setAppPage } from '@actions/appStateActionCreator';
import { appPages } from '@constants';

import HomeIcon from '@material-ui/icons/Home';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Surface from './common/Surface.react';
import { signOut } from '@actions/loginActionCreator';
import { useHistory, useLocation } from 'react-router-dom';

const {
  HOME, PROFILE, SETTING,
} = appPages;

const useStyles = makeStyles((theme) => ({
  button: {
    minHeight: theme.spacing(6),
    fontSize: theme.spacing(2),
    fontWeight: 'bold',
  },
}));

const Menu = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const classes = useStyles();
  const [menuAnchorEl, setMenuAnchorEl] = React.useState(null);

  const user = useSelector((state) => state.currentUser.currentUser);

  const appPage = (() => {
    if (location.pathname === '/home') return HOME;
    if (location.pathname === `/${user.username}`) return PROFILE;
    return null;
  })();

  const changeAppPage = (page) => {
    dispatch(setAppPage(page));
    switch (page) {
      case HOME:
        history.push('/home');
        break;
      case PROFILE:
        history.push(`/${user.username}`);
        break;
      case SETTING:
        // todo
        break;

      default:
        break;
    }
  };

  return (
    <>
      <Box
        pt={2}
        height="100vh"
        position="relative"
      >
        <Box fontFamily="Satisfy !important" clone>
          <Typography variant="h5" color="secondary">SayChiz</Typography>
        </Box>
        <Box
          mt={3}
          height="30%"
          display="flex"
          flexDirection="column"
          justifyContent="space-between"
        >
          <NeuButton
            className={classes.button}
            elevation={appPage === HOME ? -1 : 1}
            textColor={appPage === HOME ? 'primary' : 'secondary'}
            onClick={() => changeAppPage(HOME)}
          >
            <HomeIcon />
            &nbsp;
            HOME
          </NeuButton>
          <NeuButton
            className={classes.button}
            elevation={appPage === PROFILE ? -1 : 1}
            textColor={appPage === PROFILE ? 'primary' : 'secondary'}
            onClick={() => changeAppPage(PROFILE)}
          >
            <AccountCircleIcon />
            &nbsp;
            PROFILE
          </NeuButton>
          <NeuButton
            className={classes.button}
            elevation={appPage === SETTING ? -1 : 1}
            textColor={appPage === SETTING ? 'primary' : 'secondary'}
            onClick={() => changeAppPage(SETTING)}
          >
            <SettingsIcon />
            &nbsp;
            SETTING
          </NeuButton>
        </Box>
        <Box
          position="absolute"
          padding={1}
          bottom={80}
          width="100%"
          onClick={(e) => setMenuAnchorEl(e.currentTarget)}
          css={{
            '&:hover': {
              cursor: 'pointer',
            },
          }}
        >
          <Surface elevation={0.5}>
            <Box
              clone
              width="100%"
            >
              <Grid container spacing={1}>
                <Grid item xs={3} container justify="center" alignItems="center">
                  <Avatar src={user.avatar} />
                </Grid>
                <Grid item xs container direction="column" justify="center" alignItems="flex-start">
                  <Typography variant="body1" color="textPrimary">{user.nickname}</Typography>
                  <Typography variant="caption" color="textSecondary">{`@${user.username}`}</Typography>
                </Grid>
                <Grid item xs={2} container justify="center" alignItems="center">
                  <ExpandMoreIcon color="secondary" />
                </Grid>
              </Grid>
            </Box>
          </Surface>
        </Box>
      </Box>
      <MuiMenu
        open={!!menuAnchorEl}
        anchorEl={menuAnchorEl}
        onClose={() => setMenuAnchorEl(null)}
      >
        <MenuItem onClick={() => dispatch(signOut())}>
          <ExitToAppIcon />
          &nbsp;
          Logout
        </MenuItem>
      </MuiMenu>
    </>
  );
};

export default Menu;
