import { takeLatest, put, call } from 'redux-saga/effects';
import { setAreCommentsLoading, setCommentList, getPostComments } from '@actions/postDetailActionCreator';
import * as Api from '@api';

function* worker({ payload }) {
  const { postId } = payload;
  yield put(setAreCommentsLoading(true));
  try {
    const { data: { results } } = yield call(Api.getPostComments, postId);
    yield put(setCommentList(results));
  } catch (e) {
    if (e.status === 404) {
      yield put(setCommentList([]));
    }
  } finally {
    yield put(setAreCommentsLoading(false));
  }
}

export default function* getPostCommentsSaga() {
  yield takeLatest(getPostComments, worker);
}
