import {
  put, call, takeLatest,
} from 'redux-saga/effects';
import * as Api from '../../utils/Api';
import {
  getNextVisitedUserFollowers, setVisitedUserError, setNextFollowersPage, addVisitedUserFollowers,
} from '../../actions/visitedUserActionCreator';

function* worker({ payload }) {
  yield put(setVisitedUserError(false));
  try {
    const result = yield call(Api.getNextVisitedUserFollowers, payload);
    const { next, results } = result.data;
    yield put(setNextFollowersPage(next));
    yield put(addVisitedUserFollowers(results));
  } catch (e) {
    if (e.status === 404) {
      yield put(setVisitedUserError(true));
    }
  }
}

export default function* getNextUserFollowersSaga() {
  yield takeLatest(getNextVisitedUserFollowers, worker);
}
