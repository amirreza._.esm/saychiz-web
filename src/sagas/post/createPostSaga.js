import { takeLatest, call, put } from 'redux-saga/effects';
import { createPost } from '@actions/postActionCreator';
import * as Api from '@api';
import { loadTimelinePosts } from '@actions/timeLineActionCreator';

function* worker({ payload }) {
  try {
    const {
      authorId, caption, file, fileName, fileType, duration,
    } = payload;
    yield call(Api.createPost, authorId, caption, file, fileName, fileType, duration);
    yield put(loadTimelinePosts());
  } catch (e) {
    console.log('error', e);
  }
}

export default function* createPostSaga() {
  yield takeLatest(createPost, worker);
}
