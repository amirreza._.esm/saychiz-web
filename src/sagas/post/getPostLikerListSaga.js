
import { takeLatest, put, call } from 'redux-saga/effects';
import {
  setLikersList, getPostLikersList,
} from '@actions/postDetailActionCreator';
import * as Api from '@api';

function* worker({ payload }) {
  const { postId } = payload;
  try {
    const { data: { results } } = yield call(Api.getPostLikerList, postId);
    yield put(setLikersList(results));
  } catch (e) {
    if (e.status === 404) {
      yield put(setLikersList(null));
    }
  }
}

export default function* getPostLikerListSaga() {
  yield takeLatest(getPostLikersList, worker);
}
