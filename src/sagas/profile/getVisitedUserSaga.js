import {
  put, call, takeLatest,
} from 'redux-saga/effects';
import * as Api from '../../utils/Api';
import { visitedUserData, setVisitedUser, setVisitedUserError } from '../../actions/visitedUserActionCreator';

function* worker({ payload }) {
  yield put(setVisitedUserError(false));
  try {
    const resault = yield call(Api.visitedUserData, payload);
    const user = resault.data;
    yield put(setVisitedUser(user));
  } catch (e) {
    if (e.status === 404) {
      yield put(setVisitedUserError(true));
    }
  }
}

export default function* getVisitedUserSaga() {
  yield takeLatest(visitedUserData, worker);
}
