/* eslint-disable import/prefer-default-export */
export const isCloseToMaxScroll = (el, threshold) => {
  const { scrollHeight, scrollTop, offsetHeight } = el;
  if (scrollHeight - scrollTop - offsetHeight < threshold) return true;
  return false;
};
