import { handleActions } from 'redux-actions';
import {
  setIsInitiated, setIsLoggedIn, setIsUserVerified, setAppPage,
} from '@actions/appStateActionCreator';
import { appPages } from '@constants';

const initialState = {
  isInitiated: false,
  isLoggedIn: false,
  isUserVerified: false,
  // isLoggedIn: true, // test
  // isUserVerified: true,
  appPage: appPages.HOME,
};

export default handleActions({
  [setIsInitiated]: (state, { payload }) => ({ ...state, isInitiated: payload }),
  [setIsLoggedIn]: (state, { payload }) => ({ ...state, isLoggedIn: payload }),
  [setIsUserVerified]: (state, { payload }) => ({ ...state, isUserVerified: payload }),
  [setAppPage]: (state, { payload }) => ({ ...state, appPage: payload }),

}, initialState);
