import React, { useEffect, useRef } from 'react';
import CreatePost from './postComponents/CreatePost.react';
import { Box } from '@material-ui/core';
import PostList from './postComponents/PostList.react';
import { useSelector, useDispatch } from 'react-redux';
import { loadTimelinePosts, timelineLoadMore } from '@actions/timeLineActionCreator';
import { isCloseToMaxScroll } from '@utils/scrollUtils';
import Surface from './common/Surface.react';

const Home = () => {
  const dispatch = useDispatch();
  const { id } = useSelector((state) => state.currentUser.currentUser);
  const posts = useSelector((state) => state.timeLine.posts);
  const scrollContainerRef = useRef();

  const handleScrollChange = () => {
    const scrollContainer = scrollContainerRef.current;
    if (scrollContainer) {
      if (isCloseToMaxScroll(scrollContainer, 25)) {
        dispatch(timelineLoadMore());
      }
    }
  };

  useEffect(() => {
    dispatch(loadTimelinePosts(id));
  }, [dispatch, id]);

  return (
    <Box
      px={3}
      py={4}
      height="100vh"
      boxSizing="border-box"
      css={{ overflowY: 'scroll' }}
      ref={scrollContainerRef}
      onScroll={handleScrollChange}
    >
      <Box clone>
        <CreatePost />
      </Box>
      <Box mt={2}>
        <Surface>
          <PostList posts={posts} />
        </Surface>
      </Box>
    </Box>
  );
};

export default Home;
