import {
  put, call, takeLatest,
} from 'redux-saga/effects';
import * as Api from '../../utils/Api';
import {
  getNextVisitedUserFollowing, setVisitedUserError, setNextFollowingPage, addVisitedUserFollowing,
} from '../../actions/visitedUserActionCreator';

function* worker({ payload }) {
  yield put(setVisitedUserError(false));
  try {
    const result = yield call(Api.getNextVisitedUserFollowing, payload);
    const { next, results } = result.data;
    yield put(setNextFollowingPage(next));
    yield put(addVisitedUserFollowing(results));
  } catch (e) {
    if (e.status === 404) {
      yield put(setVisitedUserError(true));
    }
  }
}

export default function* getNextUserFollowingSaga() {
  yield takeLatest(getNextVisitedUserFollowing, worker);
}
