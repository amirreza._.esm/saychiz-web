import { takeEvery, call, put } from 'redux-saga/effects';
import { unlikePost } from '@actions/postActionCreator';
import * as Api from '@api';
import { loadTimelinePosts } from '@actions/timeLineActionCreator';

// not finished
function* worker({ payload }) {
  try {
    const { postId } = payload;
    yield call(Api.unlikePost, postId);
    yield put(loadTimelinePosts());
  } catch (e) {
    console.log(e);
  }
}

export default function* unlikePostSaga() {
  yield takeEvery(unlikePost, worker);
}
