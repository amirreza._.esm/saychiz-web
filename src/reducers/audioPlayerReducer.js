import { handleActions } from 'redux-actions';
import {
  setUrl, setPlayerPostId, setPlayerIsPlaying, setPlayerCurrentTimeHandler, setPlayerSeekCurrentTime,
} from '@actions/audioPlayerActionCreator';
import { appConfig } from '@constants';

const initialState = {
  url: '',
  postId: null,
  isPlaying: false,
  currentTimeHandler: null,
  seekCurrentTime: -1,
};

export default handleActions({
  [setUrl]: (state, { payload }) => ({ ...state, url: `${appConfig.serverBaseUrl}${payload}` }),
  [setPlayerPostId]: (state, { payload }) => ({ ...state, postId: payload }),
  [setPlayerIsPlaying]: (state, { payload }) => ({ ...state, isPlaying: payload }),
  [setPlayerCurrentTimeHandler]: (state, { payload }) => ({ ...state, currentTimeHandler: payload }),
  [setPlayerSeekCurrentTime]: (state, { payload }) => ({ ...state, seekCurrentTime: payload }),
}, initialState);
