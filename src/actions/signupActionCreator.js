import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  SIGNUP_SERVER_ERROR,
  SIGNUP_SET_PENDING,
  SIGNUP_SET_COUNTRY_LIST,
  SIGNUP_SET_USERNAME_AVAILABLE,
  SIGNUP_SET_EMAIL_AVAILABLE,
  SAGA_SIGNUP,
  SAGA_UPDATE_COUNTRY_LIST,
  SAGA_CHECK_USERNAME_AVAILABILITY,
  SAGA_CHECK_EMAIL_AVAILABILITY,
  SAGA_RESEND_VERIFY_EMAIL,
} = ActionTypes;


export const setSignupServerError = createAction(SIGNUP_SERVER_ERROR);
export const setSignupPending = createAction(SIGNUP_SET_PENDING);
export const setCountryList = createAction(SIGNUP_SET_COUNTRY_LIST);
export const setUsernameAvailable = createAction(SIGNUP_SET_USERNAME_AVAILABLE);
export const setEmailAvailable = createAction(SIGNUP_SET_EMAIL_AVAILABLE);

// ---sagas
export const onSignup = createAction(SAGA_SIGNUP);
export const updateCountryList = createAction(SAGA_UPDATE_COUNTRY_LIST);
export const checkUsernameAvailable = createAction(SAGA_CHECK_USERNAME_AVAILABILITY);
export const checkEmailAvailable = createAction(SAGA_CHECK_EMAIL_AVAILABILITY);
export const resendVerifyEmail = createAction(SAGA_RESEND_VERIFY_EMAIL);
