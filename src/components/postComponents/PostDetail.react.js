import React, { useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Surface from '../common/Surface.react';
import Post from './Post.react';
import { useSelector, useDispatch } from 'react-redux';
import { getPostComments, getPost, getPostLikersList } from '@actions/postDetailActionCreator';
import {
  Divider, CircularProgress, Box, IconButton, Typography, Button, Avatar, makeStyles,
} from '@material-ui/core';
import { motion } from 'framer-motion';
import { openLikerListModal, setLikerList } from '@actions/likerListModalActionCreator';

const useStyles = makeStyles((theme) => ({
  avatarItems: {
    width: theme.spacing(2),
    height: theme.spacing(2),
  },
}));

const postVariants = {
  initial: {
    y: 50,
    opacity: 0,
  },
  idle: {
    y: 0,
    opacity: 1,
  },
};

const PostDetail = () => {
  const { username, postId } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();
  const classes = useStyles();

  const {
    post, commentList, likersList, isPostLoading, areCommentsLoading,
  } = useSelector((state) => state.postDetail);

  const postNotAvailable = (!isPostLoading && !post) || (post && post.author.username !== username);

  const handleGoBack = () => {
    history.goBack();
  };

  const handleOpenLikerList = () => {
    dispatch(openLikerListModal());
  };

  useEffect(() => {
    dispatch(setLikerList(likersList));
  }, [dispatch, likersList]);

  useEffect(() => {
    dispatch(getPost({ postId }));
    dispatch(getPostComments({ postId }));
    dispatch(getPostLikersList({ postId }));
  }, [dispatch, postId]);

  const AvatarList = [];
  if (post && likersList) {
    for (let i = 0; i < post.countLike; i += 1) {
      const userWhoLiked = likersList[i];
      AvatarList.push(
        i < likersList.length
          ? <Avatar src={userWhoLiked?.avatar} className={classes.avatarItems} />
          : <div />,
      );
    }
  }

  return (
    <Box px={3} py={4} height="100vh" boxSizing="border-box" css={{ overflowY: 'scroll' }}>
      <Surface>
        <Box
          display="flex"
          alignItems="center"
        >
          <IconButton onClick={handleGoBack}>
            <ArrowBackIcon />
          </IconButton>
          <Typography color="textPrimary">{!postNotAvailable ? `${username}'s post` : "Couldn't find post"}</Typography>
        </Box>
        {!postNotAvailable && <Divider variant="middle" />}
        {!postNotAvailable && post && (
          <motion.div
            key={post.id}
            variants={postVariants}
            initial="initial"
            animate="idle"
            transition={{ type: 'tween' }}
          >
            <Post post={post} />
            {likersList && !!likersList.length && (
              <Box
                display="flex"
                alignItems="center"
                ml={2}
              >
                <Button variant="text" size="small" onClick={handleOpenLikerList}>
                  User&apos;s who liked this post
                  <AvatarGroup max={3}>
                    {AvatarList}
                  </AvatarGroup>
                </Button>
              </Box>
            )}
          </motion.div>
        )}
        {!postNotAvailable
        && (areCommentsLoading
          ? <CircularProgress />
          : (
            commentList
              && (
              <>
                { commentList.length ? (
                  <>
                    <Divider variant="middle" />
                    <Box padding={1} pl={2}>
                      <Typography color="textSecondary">Comments</Typography>
                    </Box>
                  </>
                ) : (
                  <>
                    <Divider variant="middle" />
                    <Box padding={1} pl={3}>
                      <Typography color="textSecondary">This post has no comments</Typography>
                    </Box>
                  </>
                ) }
                {
                 commentList.map((comment, index) => (
                   <div key={comment.id}>
                     <Post post={comment} clickable />
                     {index !== commentList.length - 1 && <Divider variant="inset" />}
                   </div>
                 ))
              }
              </>
              )
          ))}
      </Surface>
    </Box>
  );
};

export default PostDetail;
