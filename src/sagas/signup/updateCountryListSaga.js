import {
  put, call, takeEvery,
} from 'redux-saga/effects';
import * as Api from '@api';
import { setCountryList, updateCountryList } from '@actions/signupActionCreator';

function* worker() {
  yield put(setCountryList([]));
  try {
    const { data: list } = yield call(Api.getSignUpCountryList);
    yield put(setCountryList(list));
  } catch (e) {

  }
}

export default function* updateCountryListSaga() {
  yield takeEvery(updateCountryList, worker);
}
