import React from 'react';
import Post from './Post.react';
import { Divider } from '@material-ui/core';


const PostList = ({ posts }) => {
  return (
    posts && posts.map((post, index) => (
      <div key={post.id}>
        <Post post={post} clickable />
        {index !== posts.length - 1 && <Divider variant="middle" />}
      </div>
    ))
  );
};

export default PostList;
