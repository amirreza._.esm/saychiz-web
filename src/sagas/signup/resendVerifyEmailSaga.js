import { call, takeEvery } from 'redux-saga/effects';
import { resendVerifyEmail } from '@actions/signupActionCreator';
import * as Api from '@api';

function* worker({ payload }) {
  const userId = payload;
  try {
    yield call(Api.resendVerifyEmail, userId);
  } catch (e) {
    // todo
  }
}

export default function* resendVerifyEmailSaga() {
  yield takeEvery(resendVerifyEmail, worker);
}
