import {
  put, call, takeLatest,
} from 'redux-saga/effects';
import * as Api from '../../utils/Api';
import {
  getVisitedUserFollowers, setVisitedUserFollowers, setVisitedUserError, setNextFollowersPage,
} from '../../actions/visitedUserActionCreator';

function* worker({ payload }) {
  yield put(setVisitedUserError(false));
  try {
    const result = yield call(Api.visitedUserFollowersData, payload);
    const { next, results } = result.data;
    yield put(setNextFollowersPage(next));
    yield put(setVisitedUserFollowers(results));
  } catch (e) {
    if (e.status === 404) {
      yield put(setVisitedUserError(true));
    }
  }
}

export default function* getVisitedUserFollowersSaga() {
  yield takeLatest(getVisitedUserFollowers, worker);
}
