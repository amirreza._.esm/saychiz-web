import { createAction } from 'redux-actions';
import { ActionTypes } from '@constants';

const {
  LOGIN_ERROR,
  LOGIN_SET_PENDING,
  SAGA_ON_LOGIN,
  SAGA_AUTHENTICATE,
  SAGA_SIGN_OUT,
} = ActionTypes;

export const onLogin = createAction(SAGA_ON_LOGIN);

export const signOut = createAction(SAGA_SIGN_OUT);

export const authenticate = createAction(SAGA_AUTHENTICATE);

export const setLoginError = createAction(LOGIN_ERROR);
export const setLoginPending = createAction(LOGIN_SET_PENDING);
