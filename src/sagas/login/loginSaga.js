import { put, call, takeEvery } from 'redux-saga/effects';
import { retrieveToken, setAPIToken, authByToken } from '@api';
import { setAuthToken, setCurrentUser } from '@actions/currentUserActionCreator';
import { setIsLoggedIn, setIsUserVerified, setIsInitiated } from '@actions/appStateActionCreator';
import { onLogin, setLoginPending } from '@actions/loginActionCreator';

import { setVerificationCode } from '@actions/verificationActionCreator';

import * as Api from '@api';

function* loginWorkerSaga() {
  yield put(setIsInitiated(false));
  const token = yield call(retrieveToken);
  try {
    if (token) {
      yield put(setAuthToken(token));
      setAPIToken(token);

      const results = yield call(authByToken);
      const user = results.data;
      yield put(setCurrentUser(user));
      yield put(setIsLoggedIn(true));
      yield put(setIsLoggedIn(true));
      if (user.verifyStatus) {
        yield put(setIsUserVerified(true));
      } else {
        const { data: { code: verifyCode } } = yield call(Api.resendVerifyEmail, user.id);
        yield put(setIsUserVerified(false));
        yield put(setVerificationCode(verifyCode));
      }
    } else {
      yield put(setIsLoggedIn(false));
    }
  } catch (e) {
    yield put(setIsLoggedIn(false));
    yield put(setLoginPending(false));
  } finally {
    yield put(setIsInitiated(true));
  }
}

export default function* loginSaga() {
  yield takeEvery(onLogin, loginWorkerSaga);
}
