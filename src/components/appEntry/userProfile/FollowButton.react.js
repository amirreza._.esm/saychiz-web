/* eslint-disable no-nested-ternary */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';
import NeuButton from '../../neumorphism/NeuButton.react';

const FollowButton = () => {


  const currentUserFollowers = useSelector((state) => state.currentUser.currentUserFollowers);
  const currentUserFollowing = useSelector((state) => state.currentUser.currentUserFollowing);
  const user = useSelector((state) => state.currentUser.currentUser);

  return (
    <div>

      {
        currentUserFollowing.filter((users) => (users.username === user.username)).length !== 0
          ? <NeuButton textColor="primary" fullWidth>Following</NeuButton>
          : currentUserFollowers.filter((users) => (users.username === user.username)).length !== 0
            ? <NeuButton textColor="primary" fullWidth>Follow Back</NeuButton>
            : <NeuButton textColor="primary" fullWidth>Follow</NeuButton>
        }
    </div>
  );
};

export default FollowButton;
