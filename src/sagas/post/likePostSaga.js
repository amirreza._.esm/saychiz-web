import {
  takeEvery, call, put,
} from 'redux-saga/effects';
import { likePost } from '@actions/postActionCreator';
import * as Api from '@api';
import { loadTimelinePosts } from '@actions/timeLineActionCreator';

// not finished
function* worker({ payload }) {
  try {
    const { postId, authorId } = payload;
    yield call(Api.likePost, postId, authorId);
    yield put(loadTimelinePosts());
  } catch (e) {
    console.log(e);
  }
}

export default function* likePostSaga() {
  yield takeEvery(likePost, worker);
}
