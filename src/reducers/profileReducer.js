import { handleActions } from 'redux-actions';
import { setProfilePosts, appendProfilePosts, setProfileNextPageUrl } from '@actions/profileActionCreator';

const initialState = {
  posts: [],
  nextPageUrl: null,
};

export default handleActions({
  [setProfilePosts]: (state, { payload }) => ({ ...state, posts: payload }),
  [appendProfilePosts]: (state, { payload }) => ({
    ...state,
    posts: [...state.posts, ...payload],
  }),
  [setProfileNextPageUrl]: (state, { payload }) => ({ ...state, nextPageUrl: payload }),
}, initialState);
