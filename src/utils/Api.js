/* eslint-disable no-console */
import axios from 'axios';
import { appConfig } from '@constants';

const API = axios.create({
  baseURL: appConfig.serverBaseUrl,
});

// Auth token
export const setAPIToken = (token) => {
  if (token) {
    API.defaults.headers.Authorization = `token ${token}`;
  } else {
    API.defaults.headers.Authorization = '';
  }
};

export const storeToken = (token) => {
  setAPIToken(token);
  localStorage.setItem('secret_token', token);
};

export const retrieveToken = () => localStorage.getItem('secret_token');

export const clearToken = () => {
  setAPIToken('');
  localStorage.removeItem('secret_token');
};

API.interceptors.request.use(
  (res) => {
    appConfig.logApiCalls && console.log('req: ', res);
    return res;
  }, (err) => {
    // appConfig.logApiCalls && console.log('fail: ', err.response);
    throw err.response;
  },
);

API.interceptors.response.use(
  (res) => {
    appConfig.logApiCalls && console.log('success: ', res);
    return res;
  }, async (err) => {
    appConfig.logApiCalls && console.log('fail: ', err.response);
    const { response } = err;
    // todo: change string to error code
    if (response.status === 401 && response.data.detail === 'Token has expired') {
      appConfig.logApiCalls && console.log('reviving token...');
      const { data } = await API.get('token/');
      if (data.token) {
        setAPIToken(data.token);
      }
    }
    if (response.status === 401 && response.data.detail === 'Invalid Token') {
      appConfig.logApiCalls && console.log('deleting token...');
      clearToken();
    }
    throw err.response;
  },
);

// Api calls

export const authenticate = (usernameEmail, password) => {
  const reqInfo = { username_email: usernameEmail, password };
  return API.post('account/login/', reqInfo);
};

export const signup = (
  username, email, gender, phone, birthday, country, nickname, password,
) => {
  const reqInfo = {
    username, email, gender, phone, birthday, country, nickname, password,
  };
  return API.post('account/sign_up/', reqInfo);
};

export const getSignUpCountryList = () => API.get('account/sign_up/info/');

export const validateUsername = (username) => API.get('account/sign_up/check/', { params: { username } });

export const validateEmail = (email) => API.get('account/sign_up/check/', { params: { email } });

export const verifyEmail = (userId) => API.get(`account/sign_up/confirm_verify_email/${userId}`);

export const resendVerifyEmail = (userId) => API.get(`account/sign_up/send_email_again/${userId}`);
// get visiting user data
export const visitedUserData = (username) => API.get(`account/user_profile/get_user_by_username/?username=${username}`);
// get visiting user followers
export const visitedUserFollowersData = (id) => API.get(`follow-block/follow/${id}/get_list/?is_accepted=true&is_follower=true`);
// get visiting user following
export const visitedUserFollowingData = (id) => API.get(`follow-block/follow/${id}/get_list/?is_accepted=true&is_follower=false`);
// get next followers page
export const getNextVisitedUserFollowers = (url) => API.get(url);
// get next following page
export const getNextVisitedUserFollowing = (url) => API.get(url);
// follow users
export const follow = (followingId, followerId) => {
  const reqInfo = {
    follower_user: followerId,
    following_user: followingId,
  };
  return API.post('follow-block/follow/', reqInfo);
};

export const unfollow = (followingId, followerId) => API.delete(`follow-block/follow/${followingId}/${followerId}`);

export const getUserslist = (url) => API.get(url);
window.api = { validateEmail, validateUsername };

export const getUserInfo = (userId) => {
  return API.get(`account/user_profile/${userId}/`);
};

export const createPost = (authorId, caption, file, fileName, fileType, duration) => {
  const formData = new FormData();
  formData.append('author', authorId);
  formData.append('caption', caption);
  formData.append('audioFile', file, fileName);
  formData.append('file_type', fileType);
  formData.append('duration', duration);

  return API.post('post/create_post/', formData);
};

export const createComment = (
  sourcePostId, authorId, caption, file, fileName, fileType, duration,
) => {
  const formData = new FormData();
  formData.append('author', authorId);
  formData.append('caption', caption);
  formData.append('audioFile', file, fileName);
  formData.append('file_type', fileType);
  formData.append('duration', duration);

  return API.post(`post/create_comment/${sourcePostId}/`, formData);
};

export const deletePost = (postId) => {
  return API.delete(`post/delete_post/${postId}/`);
};

export const likePost = (postId, authorId) => {
  const body = {
    post: postId,
    user: authorId,
  };
  return API.post('post/create_like/', body);
};

export const unlikePost = (postId) => {
  return API.delete(`post/unlike/${postId}/`);
};

export const getUserPosts = (userId) => {
  return API.get(`post/posts/${userId}/`);
};

export const userPostsLoadMore = (url) => {
  return API.get(url);
};

export const authByToken = () => {
  return API.get('account/user_profile/get_user_by_token/');
};

export const signOut = () => {
  return API.get('account/logout/');
};

export const getPostComments = (postId) => {
  return API.get(`post/comments/${postId}/`);
};

export const getPostById = (postId) => {
  return API.get(`post/get_post/${postId}/`);
};

export const getPostLikerList = (postId) => {
  return API.get(`post/likes/${postId}/`);
};

export const searchByQuery = (query) => {
  return API.get('search/search-user/', {
    params: {
      term: query,
    },
  });
};

export const getUsersTimeline = (userId) => {
  return API.get(`timeline/feed/${userId}/`);
};

export const timelineLoadMore = (url) => {
  return API.get(url);
};

export const searchLoadMore = (url) => {
  return API.get(url);
};
