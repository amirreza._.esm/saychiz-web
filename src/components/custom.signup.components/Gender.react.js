import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import NeuTextField from '../neumorphism/NeuTextField.react';

const useStyles = makeStyles(() => ({
  formControl: {
    minWidth: 120,
    width: '100%',
  },
}));

export default function GenderSelect(prop) {
  const classes = useStyles();
  const [gender, setGender] = React.useState('');

  const handleChange = (event) => {
    setGender(event.target.value);
    prop.callbackFromParent(event.target.value);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">{prop.lid}</InputLabel>
        <NeuTextField
          select
          label="Gender"
          value={gender}
          margin="dense"
          fullWidth
          onChange={handleChange}
          required
          error={prop.error}
        >
          <MenuItem value="1">Male</MenuItem>
          <MenuItem value="2">Female</MenuItem>
          <MenuItem value="3">Others</MenuItem>
        </NeuTextField>
      </FormControl>
    </div>
  );
}
