import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App.react';
import * as serviceWorker from './serviceWorker';
import './assets/fonts/satisfy/satisfy.css';
// import './assets/fonts/dosis/dosis.css';
import './assets/fonts/roboto/roboto.css';

ReactDOM.render(
  // <React.StrictMode>
  <App />,
  // </React.StrictMode>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
