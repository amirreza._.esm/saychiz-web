import { handleActions } from 'redux-actions';
import {
  setSignupServerError, setSignupPending, setCountryList, setUsernameAvailable, setEmailAvailable,
} from '@actions/signupActionCreator';
import { stateModes } from '../constants/AppConstants';


/*
* states: ['idle', 'pending', 'success', 'fail']
*/
const initialState = {
  serverError: null,
  isPending: false,
  countryList: [],
  isUsernameAvailable: stateModes.IDLE,
  isEmailAvailable: stateModes.IDLE,
};

export default handleActions({
  [setSignupServerError]: (state, { payload }) => ({ ...state, serverError: payload }),
  [setSignupPending]: (state, { payload }) => ({ ...state, isPending: payload }),
  [setCountryList]: (state, { payload }) => ({ ...state, countryList: payload }),
  [setUsernameAvailable]: (state, { payload }) => ({ ...state, isUsernameAvailable: payload }),
  [setEmailAvailable]: (state, { payload }) => ({ ...state, isEmailAvailable: payload }),
}, initialState);
