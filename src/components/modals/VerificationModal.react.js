import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  makeStyles, Grid, Typography, TextField, fade, DialogTitle, Box, IconButton,
} from '@material-ui/core';
import * as PropTypes from 'prop-types';
import { closeVerificationModal, verifyUser } from '@actions/verificationActionCreator';
import NeuDialog from '../neumorphism/NeuDialog.react';
import NeuButton from '../neumorphism/NeuButton.react';
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';
import { signOut } from '@actions/loginActionCreator';

const useStyles = makeStyles((theme) => ({
  modalBody: {
    backdropFilter: 'blur(20px)',
    backgroundColor: fade(theme.palette.background.paper, 0.4),
  },
}));

const VerificationModalBody = ({ handleClose }) => {
  const classes = useStyles();

  const [enteredCode, setEnteredCode] = useState('');

  const dispatch = useDispatch();
  const user = useSelector((state) => state.currentUser.currentUser);
  const verificationCode = useSelector((state) => state.verification.verificationCode);
  const error = useSelector((state) => state.verification.error);
  const isPending = useSelector((state) => state.verification.isPending);

  const handleConfirmCode = () => {
    dispatch(verifyUser({ id: user.id, verificationCode, enteredCode: parseInt(enteredCode, 10) }));
  };

  return (
    <Grid
      className={classes.modalBody}
      container
      direction="column"
    >
      <Grid item>
        <DialogTitle>
          <Grid container>
            <Grid item xs />
            <Grid item>
              <IconButton onClick={handleClose}>
                <CloseOutlinedIcon />
              </IconButton>
            </Grid>
          </Grid>
        </DialogTitle>
      </Grid>
      <Box m="0px !important" clone>
        <Grid
          item
          xs
          container
          direction="column"
          justify="center"
          alignItems="center"
          spacing={10}
        >
          <Grid item>
            <Typography variant="h4" color="primary">Verify your account</Typography>
          </Grid>
          <Grid
            item
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item>
              <Typography variant="body2" color="textPrimary">Please enter the verification code we have sent to your email.</Typography>
            </Grid>
            <Grid item>
              <TextField
                inputProps={{
                  style: { textAlign: 'center' },
                }}
                variant="outlined"
                size="small"
                value={enteredCode}
                onChange={(e) => setEnteredCode(e.target.value.trim())}
                helperText={error}
                error={error}
              />
            </Grid>
          </Grid>
          <Grid item>
            <NeuButton pending={isPending} textColor="primary" onClick={handleConfirmCode}>Confirm</NeuButton>
          </Grid>
        </Grid>
      </Box>
    </Grid>
  );
};

VerificationModalBody.propTypes = {
  handleClose: PropTypes.func,
};

const VerificationModal = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.modals.verificationIsOpen);
  const handleClose = () => {
    dispatch(signOut());
    dispatch(closeVerificationModal());
  };

  return (
    <NeuDialog
      open={isOpen}
      onClose={handleClose}
      borderRadius={5}
    >
      <VerificationModalBody
        handleClose={handleClose}
      />
    </NeuDialog>
  );
};

export default VerificationModal;
