import {
  put, call, takeLatest,
} from 'redux-saga/effects';
import * as Api from '../../utils/Api';
import {
  addCurrentUserFollowers, getAllCurrentUserFollowers, setCurrentUserFollowers,
} from '../../actions/currentUserActionCreator';

function* worker({ payload }) {
  try {
    let { data: { next, previous, results } } = yield call(Api.getUserslist, payload);
    yield put(setCurrentUserFollowers(results));
    while (next) {
      const xxx = yield call(Api.getUserslist, next);
      next = xxx.data.next;
      previous = xxx.data.previous;
      results = xxx.data.results;
      if (previous) {
        yield put(addCurrentUserFollowers(results));
      } else {
        yield put(setCurrentUserFollowers(results));
      }
    }
  } catch (e) {
    console.log('error');
  }
}

export default function* getCurrentUserFollowersSaga() {
  yield takeLatest(getAllCurrentUserFollowers, worker);
}
