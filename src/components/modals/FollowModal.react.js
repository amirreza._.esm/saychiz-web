import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  makeStyles, Grid, Typography, fade, DialogTitle, Box, IconButton,
} from '@material-ui/core';
import * as PropTypes from 'prop-types';
import { closeFollowModal } from '@actions/followActionCreator';
import NeuDialog from '../neumorphism/NeuDialog.react';
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';

const useStyles = makeStyles((theme) => ({
  modalBody: {
    backdropFilter: 'blur(20px)',
    backgroundColor: fade(theme.palette.background.paper, 0.4),
  },
  modalheader: {
    borderBottom: '2px solid black',
  },

}));

const FollowModalBody = ({ handleClose }) => {
  const classes = useStyles();


  return (
    <Grid
      className={classes.modalBody}
      container
      direction="column"
    >
      <Grid item xs={12}>
        <DialogTitle
          className={classes.modalheader}
        >
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing={3}
          >
            <Grid className={classes.titleItems} item>
              <Typography variant="h4" color="primary"> </Typography>
            </Grid>
            <Grid className={classes.titleItems2} item>
              <Typography variant="h4" color="primary">Following</Typography>
            </Grid>
            <Grid className={classes.titleItems3} item>
              <IconButton onClick={handleClose}>
                <CloseOutlinedIcon />
              </IconButton>
            </Grid>
          </Grid>
        </DialogTitle>
      </Grid>
      <Box m="0px !important" clone>
        <Grid
          item
          xs
          container
          direction="column"
          justify="center"
          alignItems="center"
          spacing={10}
        >
          <h1>h111111111111111111111111111111sadadasd</h1>
        </Grid>
      </Box>
    </Grid>
  );
};

FollowModalBody.propTypes = {
  handleClose: PropTypes.func,
};

const FollowModal = () => {
  const dispatch = useDispatch();
  const isOpen = useSelector((state) => state.modals.followIsOpen);
  const handleClose = () => {
    dispatch(closeFollowModal());
  };

  return (
    <NeuDialog
      open={isOpen}
      onClose={handleClose}
      borderRadius={5}
    >
      <FollowModalBody
        handleClose={handleClose}
      />
    </NeuDialog>
  );
};

export default FollowModal;
