/* eslint-disable max-len */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Typography, Avatar, Grid, Box,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import NeuButton from '../../neumorphism/NeuButton.react';
import { useHistory } from 'react-router-dom';
import {
  visitedUserData, setVisitedUser, setNextFollowersPage, setNextFollowingPage,
} from '@actions/visitedUserActionCreator';
import { followUser, unfollowUser } from '@actions/followActionCreator';
import { getAllCurrentUserFollowing, getAllCurrentUserFollowers } from '@actions/currentUserActionCreator';
import Surface from '../../common/Surface.react';
import PostList from '../../postComponents/PostList.react';
import { loadProfilePosts, profilePostsLoadMore } from '@actions/profileActionCreator';
import { isCloseToMaxScroll } from '@utils/scrollUtils';


const UserProfile = () => {
  const useStyles = makeStyles((theme) => ({
    container: {
      padding: '50px',
    },
    Avatar: {
      width: theme.spacing(15),
      height: theme.spacing(15),
    },
    about: {
      padding: '1px',
      overflowWrap: 'break-word',
    },
    Options: {
      cursor: 'pointer',
    },
    postCount: {
      cursor: 'default',
    },
  }));

  const visitedError = useSelector((state) => state.visitedUser.visitedUserError);
  const classes = useStyles();
  const visitedUser = useSelector((state) => state.visitedUser.visitedUser);
  const currentUser = useSelector((state) => state.currentUser.currentUser);
  const currentUserFollowers = useSelector((state) => state.currentUser.currentUserFollowers);
  const currentUserFollowing = useSelector((state) => state.currentUser.currentUserFollowing);
  const dispatch = useDispatch();
  const history = useHistory();
  const visitedUsername = window.location.pathname.substr(1);

  const posts = useSelector((state) => state.profile.posts);
  const scrollContainerRef = useRef();

  const handleScrollChange = () => {
    const scrollContainer = scrollContainerRef.current;
    if (scrollContainer) {
      if (isCloseToMaxScroll(scrollContainer, 25)) {
        dispatch(profilePostsLoadMore());
      }
    }
  };

  useEffect(() => {
    if (visitedUser) {
      dispatch(loadProfilePosts(visitedUser.id));
    }
  }, [dispatch, visitedUser]);

  useEffect(() => {
    if (visitedUsername === currentUser.username) {
      dispatch(setVisitedUser(currentUser));
    } else {
      dispatch(visitedUserData(visitedUsername));
    }
    dispatch(setNextFollowersPage(false));
    dispatch(setNextFollowingPage(false));
    dispatch(getAllCurrentUserFollowing(`follow-block/follow/${currentUser.id}/get_list/?is_accepted=true&is_follower=false`));
    dispatch(getAllCurrentUserFollowers(`follow-block/follow/${currentUser.id}/get_list/?is_accepted=true&is_follower=true`));
  }, [currentUser, dispatch, visitedUsername]);

  const unfollow = (id) => {
    dispatch(unfollowUser({ visitedUser: id, currentUser: currentUser.id }));
  };

  const follow = () => {
    dispatch(followUser({ visitedUser: visitedUser.id, currentUser: currentUser.id }));
  };
  return (
    <Box
      px={3}
      py={4}
      height="100vh"
      boxSizing="border-box"
      css={{ overflowY: 'scroll' }}
      ref={scrollContainerRef}
      onScroll={handleScrollChange}
    >
      <Surface>
        <Grid container direction="column" justify="flex-start" alignItems="stretch" className={classes.container}>
          <Grid
            item
            xs
            container
            direction="column"
            justify="flex-start"
            alignItems="stretch"
          >
            <Grid container item xs direction="row" justify="center" alignItems="stretch">
              <Grid item xs={4} container direction="row" justify="center">
                <Avatar alt={visitedUser.nickname} src={visitedUser.avatar} className={classes.Avatar} />
              </Grid>
              <Grid item container direction="column" justify="flex-start" alignItems="stretch" xs={8}>
                <Grid item container direction="row" justify="flex-start" alignItems="center" xs>
                  <Grid item xs container direction="column" justify="center" alignItems="center" className={classes.postCount}>
                    <Grid item xs>
                      <Typography color="textPrimary" variant="h4">Post</Typography>
                    </Grid>
                    <Grid item xs>
                      {
                    visitedError ? <Typography color="textPrimary" variant="h6"> </Typography>
                      : <Typography color="textPrimary">{visitedUser.countPost}</Typography>
                    }
                    </Grid>
                  </Grid>
                  <Grid item xs container direction="column" justify="center" alignItems="center" onClick={() => history.push(`/${visitedUser.username}/followers`)} className={classes.Options}>
                    <Grid item xs>
                      <Typography variant="h4" color="textPrimary">Follower</Typography>
                    </Grid>
                    <Grid item xs>
                      {
                    visitedError ? <Typography color="textPrimary" variant="h6"> </Typography>
                      : <Typography color="textPrimary">{visitedUser.countFollower}</Typography>
                    }
                    </Grid>
                  </Grid>
                  <Grid item xs container direction="column" justify="center" alignItems="center" onClick={() => history.push(`/${visitedUser.username}/following`)} className={classes.Options}>
                    <Grid item xs>
                      <Typography variant="h4" color="textPrimary">Following</Typography>
                    </Grid>
                    <Grid item xs>
                      {
                    visitedError ? <Typography color="textPrimary" variant="h6"> </Typography>
                      : <Typography color="textPrimary">{visitedUser.countFollowing}</Typography>
                    }
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs>
                  {
                    visitedError ? <Typography color="textPrimary" variant="h6"> </Typography>
                      : <Typography variant="body2" className={classes.about} color="textPrimary">{visitedUser.about}</Typography>
                }
                </Grid>
              </Grid>
            </Grid>
            <Grid container item xs direction="row" justify="center" alignItems="stretch">
              <Grid item xs={4} container justify="center" direction="column" alignItems="center">
                <Grid item xs>
                  {
                    visitedError ? <Typography color="textPrimary" variant="h6">{`@${visitedUsername}`}</Typography>
                      : <Typography color="textPrimary" variant="h4">{visitedUser.nickname}</Typography>
                }
                </Grid>
                <Grid item xs>
                  {
                  visitedError ? <Typography color="textPrimary" variant="h6"> </Typography>
                    : <Typography color="textPrimary" variant="h6">{`@${visitedUser.username}`}</Typography>
                }
                </Grid>
              </Grid>
              <Grid item xs={8} container direction="column" justify="flex-start" alignItems="stretch">
                {
                currentUser.username !== visitedUser.username
                  ? (
                    <Grid item container direction="row" justify="flex-start" alignItems="center" xs spacing={2}>
                      <Grid item xs={8}>
                        {
                          visitedError ? <Typography color="textPrimary" variant="h6">This username doesnt exist</Typography>
                            : currentUserFollowing.filter((users) => (users.username === visitedUser.username)).length !== 0
                              ? <NeuButton textColor="primary" fullWidth onClick={() => unfollow(visitedUser.id)}>Following</NeuButton>
                              : currentUserFollowers.filter((users) => (users.username === visitedUser.username)).length !== 0
                                ? <NeuButton textColor="primary" fullWidth onClick={() => follow(visitedUser.id)}>Follow Back</NeuButton>
                                : <NeuButton textColor="primary" fullWidth onClick={() => follow(visitedUser.id)}>Follow</NeuButton>
                        }
                      </Grid>
                      <Grid item xs={4} container alignItems="stretch" justify="flex-start" direction="column">
                        {
                          visitedError ? <Typography> </Typography>
                            : <NeuButton textColor="primary">...</NeuButton>
                        }
                      </Grid>
                    </Grid>
                  )
                  : (
                    <Grid item xs container direction="column" justify="center" alignItems="center">
                      <NeuButton textColor="primary" fullWidth>Edit Profile</NeuButton>
                    </Grid>
                  )
              }
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Surface>
      <Box
        mt={2}
      >
        <Surface>
          <PostList posts={posts} />
        </Surface>
      </Box>
    </Box>

  );
};

export default UserProfile;
