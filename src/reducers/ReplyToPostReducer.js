import { handleActions } from 'redux-actions';
import { openReplyModal, closeReplyModal } from '@actions/ReplyToPostActionCreator';

const initialState = {
  verificationIsOpen: false,
  isOpen: false,
  sourcePostId: null,
  sourceAuthor: null,
};

export default handleActions({
  [openReplyModal]: (state, { payload }) => {
    const { sourcePostId, sourceAuthor } = payload;

    return {
      ...state, isOpen: true, sourcePostId, sourceAuthor,
    };
  },
  [closeReplyModal]: (state) => ({
    ...state, isOpen: false, sourcePostId: null, sourceAuthor: null,
  }),
}, initialState);
