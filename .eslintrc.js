module.exports = {
  env: {
    browser: true,
    es6: true,
    jest: true
  },
  "parser": "babel-eslint",
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'react-hooks',
  ],
  rules: {
    "class-methods-use-this": "off",
    "import/order": "off",
    "react/jsx-props-no-spreading": ["off"],
    "jsx-a11y/label-has-associated-control": ["error", {
      "labelComponents": ["label"],
      "labelAttributes": ["htmlFor"],
      "controlComponents": ["input"]
    }],
    "jsx-a11y/label-has-for": "off",
    "linebreak-style": "off",
    "no-unused-expressions": ["error", { "allowShortCircuit": true }],
    "no-underscore-dangle": ["error", {
      "allow": [
      ]
    }],
    "react/destructuring-assignment": "off",
    "react/forbid-prop-types": "off",
    "react/jsx-filename-extension": "off",
    "react/jsx-no-duplicate-props": ["error", { "ignoreCase": false }],
    "react/require-default-props": "off",
    "arrow-body-style": "off",
    "react/sort-comp": "off",
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "error"
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@api', './src/utils/Api'],
          ['@constants', './src/constants/AppConstants'],
          ['@assets', './src/assets'],
          ['@utils', './src/utils'],
          ['@actions', './src/actions'],
        ],
        extensions: ['.ts', '.js', '.jsx', '.json']
      }
    }
  }
};
