/* eslint-disable max-len */
import { handleActions } from 'redux-actions';
import {
  setCurrentUser, removeCurrentUser, setAuthToken, removeAuthToken,
  removeCurrentUserFollowing, removeCurrentUserFollowers,
  setCurrentUserFollowers, setCurrentUserFollowing,
  addCurrentUserFollowers, addCurrentUserFollowing,
  setNextFollowing, setNextFollowers,
  removeUserFollower, removeUserFollowing,
} from '@actions/currentUserActionCreator';

const initialState = {
  currentUser: {
    id: '3',
    username: 'zolfa',
    email: 'z.shefreie@gmail.com',
    nickname: 'zolfash',
    birthday: '1999-02-20',
    gender: '2',
    isPrivate: 'false',
    avatar: 'https://img.lovepik.com/element/40143/1122.png_860.png',
    about: 'kachal kachal kalache roghane kale pache.',
    countFollower: '100',
    countFollowing: '90',
    countPost: '2',
    verifyStatus: 'true',
    theme: '',
    country: 'Iran',
    phone: '+12223334444',
  },
  authToken: null,
  currentUserFollowers: [
    {
      username: '78m.ali',
      nickname: 'Sky',
      avatar: 'https://www.meme-arsenal.com/memes/3cbd5a9bfb11fd86f4106df9f3493a54.jpg',
      about: 'gamer',
    },
    {
      username: 'farinamHZ',
      nickname: 'fari',
      avatar: '',
      about: 'You only get one shot, do not miss your chance to blow!',
    },
    {
      username: 'blackou7',
      nickname: 'davane',
      avatar: 'https://carbonmade-media.accelerator.net/25183117;640x640.png?auto=webp',
      about: 'Remmember the game',
    },

  ],
  currentUserFollowing: [{
    username: 'farinamHZ',
    nickname: 'fari',
    avatar: '',
    about: 'You only get one shot, do not miss your chance to blow!',
  }],
  nextFollowing: null,
  nextFollowers: null,
};
export default handleActions({
  [setCurrentUser]: (state, action) => {
    const { payload } = action;
    return { ...state, currentUser: payload };
  },
  [removeCurrentUser]: (state) => ({ ...state, currentUser: null }),
  [removeCurrentUserFollowers]: (state) => ({ ...state, currentUserFollowers: null }),
  [removeCurrentUserFollowing]: (state) => ({ ...state, currentUserFollowing: null }),
  [setCurrentUserFollowers]: (state, { payload }) => ({ ...state, currentUserFollowers: payload }),
  [setCurrentUserFollowing]: (state, { payload }) => ({ ...state, currentUserFollowing: payload }),
  [addCurrentUserFollowers]: (state, { payload }) => ({ ...state, currentUserFollowers: state.currentUserFollowers.concat(payload) }),
  [addCurrentUserFollowing]: (state, { payload }) => ({ ...state, currentUserFollowing: state.currentUserFollowing.concat(payload) }),
  [removeUserFollower]: (state, { payload }) => ({ ...state, currentUserFollowers: state.currentUserFollowers.filter((user) => (user.id !== payload)) }),
  [removeUserFollowing]: (state, { payload }) => ({ ...state, currentUserFollowing: state.currentUserFollowing.filter((user) => (user.id !== payload)) }),
  [setNextFollowing]: (state, { payload }) => ({ ...state, nextFollowing: payload }),
  [setNextFollowers]: (state, { payload }) => ({ ...state, nextFollowers: payload }),
  [setAuthToken]: (state, { payload }) => ({ ...state, authToken: payload }),
  [removeAuthToken]: (state) => ({ ...state, authToken: null }),
}, initialState);
