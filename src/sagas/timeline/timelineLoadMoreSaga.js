import {
  call, select, put, takeLeading,
} from 'redux-saga/effects';
import { timelineLoadMore, setTimelineNextPageUrl, appendTimelinePosts } from '@actions/timeLineActionCreator';
import * as Api from '@api';

function* timelineLoadMoreWorkerSaga() {
  const nextPageUrl = yield select((state) => state.timeLine.nextPageUrl);
  try {
    if (nextPageUrl) {
      const {
        data: {
        // eslint-disable-next-line no-unused-vars
          count, next, previous, results,
        },
      } = yield call(Api.timelineLoadMore, nextPageUrl);

      yield put(appendTimelinePosts(results));
      yield put(setTimelineNextPageUrl(next));
    }
  } catch (e) {
    console.log(e);
  }
}

export default function* timelineLoadMoreSaga() {
  yield takeLeading(timelineLoadMore, timelineLoadMoreWorkerSaga);
}
