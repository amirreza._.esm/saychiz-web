import React, {
  useRef, useEffect, useCallback,
} from 'react';
import { Box, useTheme } from '@material-ui/core';
// import padStart from 'lodash/padStart';

// import mean from 'lodash/mean';
// import clamp from 'lodash/clamp';

const getNormalizedDataArray = (analyserRef) => {
  const analyser = analyserRef.current;
  if (analyser) {
    const dataArray = new Float32Array(analyser.fftSize);
    analyser.getFloatTimeDomainData(dataArray);

    // const max = Math.max(...dataArray);
    // return dataArray.map((n) => (n * 1.0) / (max * 1.0));
    return dataArray;
  }
  return null;
};

const getFrqDataArray = (analyserRef) => {
  const analyser = analyserRef.current;
  if (analyser) {
    const dataArray = new Float32Array(analyser.fftSize);
    analyser.getFloatFrequencyData(dataArray);
    const maxDb = analyser.maxDecibels;
    const minDb = analyser.minDecibels;
    const freqScale = 1 / (maxDb - minDb);
    const freqOffset = minDb;
    return dataArray.map((n) => freqScale * (n - freqOffset));
  }
  return null;
};


const Visualizer = ({ analyserRef, isPlaying }) => {
  const canvas = useRef(null);
  const animationRef = useRef(null);
  const theme = useTheme();

  const drawCircular = useCallback((dataArray, frqArray) => {
    const ctx = canvas.current.getContext('2d');
    const { width: w, height: h } = canvas.current.getBoundingClientRect();
    const cx = w / 2;
    const cy = h / 2;


    // const red = clamp(55 + (mean(frqArray) * 200));
    // const blue = clamp(55 + ((1 - mean(frqArray)) * 200));
    // const red = (i) => clamp(55 + (frqArray[i] * 200));
    // const blue = (i) => clamp(55 + ((1 - frqArray[i]) * 200));


    ctx.clearRect(0, 0, w, h);

    for (let i = 0; i < 360; i += 1) {
      let j = i;
      if (i > 0 && i < 90) {
        j = 180 - i;
      }
      if (i > 270 && i < 360) {
        j = 180 + (360 - i);
      }
      const angle = i / 57.2958;
      const r = 30;
      const y = cy + (Math.sin(angle) * (r + frqArray[j + 500] * 10));
      const x = cx + (Math.cos(angle) * (r + frqArray[j + 500] * 10));
      // ctx.fillStyle = `rgba(${red(500)},${0},${blue(500)},${0.2})`;
      ctx.fillStyle = theme.palette.primary.main;
      ctx.fillRect(x, y, 1, 1);
    }

    //= =-=-=-=-=-=-

    for (let i = 0; i < 360; i += 1) {
      const angle = i / 57.2958;
      const r = 50;
      const y = cy + (Math.sin(angle) * (r - 0 + dataArray[(i + 0) * 5] * 5));
      const x = cx + (Math.cos(angle) * (r - 0 + dataArray[(i + 0) * 5] * 5));
      // ctx.fillStyle = `rgba(${red(500)},${0},${blue(500)},${0.2})`;
      ctx.fillStyle = theme.palette.secondary.main;
      ctx.fillRect(x, y, 1, 1);
    }
    for (let i = 0; i < 360; i += 1) {
      const angle = i / 57.2958;
      const r = 50;
      const y = cy + (Math.sin(angle) * (r - 2 + dataArray[(i + 5) * 5] * 5));
      const x = cx + (Math.cos(angle) * (r - 2 + dataArray[(i + 5) * 5] * 5));
      // ctx.fillStyle = `rgba(${red(900)},${0},${blue(900)},${0.5})`;
      ctx.fillStyle = theme.palette.secondary.main;
      ctx.fillRect(x, y, 1, 1);
    }
    for (let i = 0; i < 360; i += 1) {
      const angle = i / 57.2958;
      const r = 50;
      const y = cy + (Math.sin(angle) * (r - 4 + dataArray[(i + 10) * 5] * 5));
      const x = cx + (Math.cos(angle) * (r - 4 + dataArray[(i + 10) * 5] * 5));
      // ctx.fillStyle = `rgba(${red(1100)},${0},${blue(1100)},${1})`;
      ctx.fillStyle = theme.palette.secondary.main;
      ctx.fillRect(x, y, 1, 1);
    }
    for (let i = 0; i < 360; i += 1) {
      const angle = i / 57.2958;
      const r = 50;
      const y = cy + (Math.sin(angle) * (r - 6 + dataArray[(i + 20) * 5] * 5));
      const x = cx + (Math.cos(angle) * (r - 6 + dataArray[(i + 20) * 5] * 5));
      // ctx.fillStyle = `rgba(${red(900)},${0},${blue(900)},${0.5})`;
      ctx.fillStyle = theme.palette.secondary.main;
      ctx.fillRect(x, y, 1, 1);
    }
    for (let i = 0; i < 360; i += 1) {
      const angle = i / 57.2958;
      const r = 50;
      const y = cy + (Math.sin(angle) * (r - 8 + dataArray[(i + 30) * 5] * 5));
      const x = cx + (Math.cos(angle) * (r - 8 + dataArray[(i + 30) * 5] * 5));
      // ctx.fillStyle = `rgba(${red(500)},${0},${blue(500)},${0.2})`;
      ctx.fillStyle = theme.palette.secondary.main;
      ctx.fillRect(x, y, 1, 1);
    }
  }, [theme.palette.primary.main, theme.palette.secondary.main]);

  const update = useCallback(() => {
    const dataArray = getNormalizedDataArray(analyserRef);
    const frqArray = getFrqDataArray(analyserRef);
    if (dataArray && frqArray) {
      drawCircular(dataArray, frqArray);
    }
    animationRef.current = requestAnimationFrame(update);
  }, [analyserRef, drawCircular]);

  useEffect(() => {
    animationRef.current = requestAnimationFrame(update);
    return () => {
      cancelAnimationFrame(animationRef.current);
    };
  }, [update]);


  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <canvas
        ref={canvas}
        width="200px"
        height="200px"
      />
    </Box>
  );
};

export default Visualizer;
