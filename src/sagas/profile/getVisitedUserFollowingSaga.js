import {
  put, call, takeLatest,
} from 'redux-saga/effects';
import * as Api from '../../utils/Api';
import {
  getVisitedUserFollowing, setVisitedUserFollowing, setVisitedUserError, setNextFollowingPage,
} from '../../actions/visitedUserActionCreator';

function* worker({ payload }) {
  yield put(setVisitedUserError(false));
  try {
    const result = yield call(Api.visitedUserFollowingData, payload);
    const { next, results } = result.data;
    yield put(setNextFollowingPage(next));
    yield put(setVisitedUserFollowing(results));
  } catch (e) {
    if (e.status === 404) {
      yield put(setVisitedUserError(true));
    }
  }
}

export default function* getVisitedUserFollowingSaga() {
  yield takeLatest(getVisitedUserFollowing, worker);
}
