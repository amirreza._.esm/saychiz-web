import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import NeuTextField from '../neumorphism/NeuTextField.react';

const useStyles = makeStyles(() => ({
  root: {
    width: '100%',
  },
}));

export default function InputAdornments(prop) {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    password: '',
    showPassword: false,
  });


  const handleChange = () => (event) => {
    setValues({ ...values, password: event.target.value });
    prop.callbackFromParent(event.target.value);
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  return (

    <FormControl className={classes.root}>
      <InputLabel htmlFor="standard-adornment-password">{prop.lid}</InputLabel>
      <NeuTextField
        error={prop.error}
        label={prop.lid}
        type={values.showPassword ? 'text' : 'password'}
        value={values.password}
        margin="dense"
        fullWidth
        onChange={handleChange('password')}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
              >
                {values.showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
    </FormControl>
  );
}
