/* eslint-disable max-len */
/* eslint-disable no-nested-ternary */
import React, { useEffect } from 'react';
import {
  Grid, Typography, Avatar,
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useHistory } from 'react-router-dom';
import NeuButton from '../../neumorphism/NeuButton.react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import { getVisitedUserFollowers, getNextVisitedUserFollowers } from '@actions/visitedUserActionCreator';
import { followUser, unfollowUser } from '@actions/followActionCreator';


const UserFollowers = () => {
  const useStyles = makeStyles(() => ({
    border: {
      borderBottomColor: 'white',
      paddingTop: '4px',
    },
    followers: {
      boxShadow: 'none',
      borderRadius: '10px',
      backgroundColor: 'rgba(255, 17, 133, 0.3)',
      '&:hover': {
        boxShadow: 'none',
        borderRadius: '10px',
        backgroundColor: 'rgba(255, 17, 133, 0.3)',
      },
    },
    otherUsers: {
      borderBottom: '2px',
    },
    followStatus: {
      background: 'gray',
      opacity: '0.5',
    },
    backIcon: {
      cursor: 'pointer',
    },
    options: {
      cursor: 'pointer',
    },
  }));

  const classes = useStyles();
  const currentUser = useSelector((state) => state.currentUser.currentUser);
  const visitedUser = useSelector((state) => state.visitedUser.visitedUser);
  const visitedUserFollowers = useSelector((state) => state.visitedUser.visitedUserFollowers);
  const currentUserFollowers = useSelector((state) => state.currentUser.currentUserFollowers);
  const currentUserFollowing = useSelector((state) => state.currentUser.currentUserFollowing);
  const nextFollowersPage = useSelector((state) => state.visitedUser.nextFollowersPage);
  const history = useHistory();
  const dispatch = useDispatch();

  const unfollow = (id) => {
    dispatch(unfollowUser({ visitedUser: id, currentUser: currentUser.id }));
  };

  const follow = (id) => {
    dispatch(followUser({ visitedUser: id, currentUser: currentUser.id }));
  };

  const showMoreUsers = () => {
    dispatch(getNextVisitedUserFollowers(nextFollowersPage));
  };

  useEffect(() => {
    dispatch(getVisitedUserFollowers(visitedUser.id));
  }, [dispatch, visitedUser.id]);
  return (
    <Grid container direction="column" justify="flex-start" alignItems="stretch">
      <Grid item xs>
        <Box borderBottom={1} className={classes.border}>
          <Grid item xs container direction="row" justify="flex-start" alignItems="stretch">
            <Grid item xs={2} container direction="row" justify="center" alignItems="center" className={classes.backIcon}>
              <ArrowBackIcon color="primary" onClick={() => history.goBack()} className={classes.backIcon} />
            </Grid>
            <Grid item xs container direction="column" justify="flex-start" alignItems="stretch">
              <Grid item xs>
                <Typography color="textPrimary" variant="h4">{visitedUser.nickname}</Typography>
              </Grid>
              <Grid item xs>
                <Typography color="textPrimary" variant="h6">{visitedUser.username}</Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs container direction="row" justify="center" alignItems="stretch">
            <Grid item xs>
              <NeuButton fullWidth onClick={() => history.push(`/${visitedUser.username}/followers`)} textColor="primary" className={classes.followers}>followers</NeuButton>
            </Grid>
            <Grid item xs>
              <NeuButton fullWidth onClick={() => history.push(`/${visitedUser.username}/following`)} textColor="primary">following</NeuButton>
            </Grid>
          </Grid>
        </Box>
      </Grid>
      <Grid item xs container direction="column" justify="flex-start" alignItems="stretch">
        <Grid item xs className={classes.otherUsers}>
          {visitedUserFollowers.map((follower) => (
            <Box borderBottom={1} className={classes.border} key={follower.username} paddingY={2}>
              <Grid item xs container dircetion="row" justify="flex-start" alignItems="stretch">
                <Grid item xs={1} container direction="column" justify="center" alignItems="center">
                  <Avatar alt={follower.nickname} src={follower.avatar} />
                </Grid>
                <Grid item xs container direction="column" justify="flex-start" alignItems="stretch">
                  <Grid item xs container direction="row" justify="flex-start" alignItems="stretch">
                    <Grid item xs container direction="column" justify="flex-start" alignItems="stretch" onClick={() => (history.push(`/${follower.username}`))} className={classes.options}>
                      <Grid item xs>
                        <Typography color="textPrimary">{follower.nickname}</Typography>
                      </Grid>
                      <Grid item xs container direction="row" justify="flex-start" alignItems="center" spacing={0}>
                        <Grid item xs>
                          <Typography color="textSecondary">{`@${follower.username}`}</Typography>
                        </Grid>
                        <Grid item xs>
                          {
                            currentUserFollowers.filter((users) => (users.username === follower.username)).length !== 0
                              ? <Typography color="textSecondary" variant="caption" className={classes.followStatus}>Follows You</Typography> : <div> </div>
                          }
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs container direction="column" justify="flex-end" alignItems="flex-end">
                      {
                         currentUserFollowing.filter((users) => (users.username === follower.username)).length !== 0
                           ? <NeuButton textColor="primary" onClick={() => unfollow(follower.id)}>Following</NeuButton>
                           : currentUserFollowers.filter((users) => (users.username === follower.username)).length !== 0
                             ? <NeuButton textColor="primary" onClick={() => follow(follower.id)}>Follow Back</NeuButton>
                             : currentUser.username !== follower.username
                               ? <NeuButton textColor="primary" onClick={() => follow(follower.id)}>Follow</NeuButton>
                               : <div>  </div>
                          }
                    </Grid>
                  </Grid>
                  <Grid item xs>
                    <Typography variant="body2" color="textPrimary">{follower.about}</Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          ))}
          {
            nextFollowersPage ? <NeuButton textColor="primary" fullWidth onClick={showMoreUsers}>Show more</NeuButton> : <div> </div>
          }
        </Grid>
      </Grid>
    </Grid>
  );
};

export default UserFollowers;
