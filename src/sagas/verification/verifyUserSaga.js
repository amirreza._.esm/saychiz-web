import { takeEvery, call, put } from 'redux-saga/effects';
import { verifyUser, setVerificationError, setVerificationPending } from '@actions/verificationActionCreator';
import { setIsUserVerified } from '@actions/appStateActionCreator';
import * as Api from '@api';


function* worker({ payload }) {
  const { id, verificationCode, enteredCode } = payload;

  yield put(setVerificationError(''));
  yield put(setVerificationPending(true));
  if (verificationCode === enteredCode) {
    try {
      yield call(Api.verifyEmail, id);
      yield put(setIsUserVerified(true));
    } catch (e) {
      yield put(setIsUserVerified(false));
      if (e) {
        if (e.status === 401) {
          yield put(setVerificationError('_DEV-MODE_Your token has expired!'));
        } else {
          yield put(setVerificationError(e.data));
        }
      } else {
        yield put(setVerificationError('Sorry! please try again.'));
      }
    } finally {
      yield put(setVerificationPending(false));
    }
  } else {
    yield put(setVerificationError('Your verification code is incorrect!'));
    yield put(setVerificationPending(false));
  }
}

export default function* verifyUserSaga() {
  yield takeEvery(verifyUser, worker);
}
