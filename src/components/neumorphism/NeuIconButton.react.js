import React from 'react';
import {
  makeStyles, useTheme, CircularProgress, IconButton,
} from '@material-ui/core';
import Color from 'color';
import * as PropTypes from 'prop-types';


const useStyles = makeStyles(() => ({
  root: (props) => ({
    ...(props.textColor && { color: props.textColor }),
    boxShadow: `${props.isInset ? 'inset' : ''} -${props.offset}px -${props.offset}px ${props.spread}px ${props.lightColor}
        , ${props.isInset ? 'inset' : ''} ${props.offset}px ${props.offset}px ${props.spread}px ${props.darkColor}`,
    borderRadius: '50%',
    backgroundColor: props.color,
    '&:hover': {
      boxShadow: `${props.isInset ? 'inset' : ''} -${props.offset * 2}px -${props.offset * 2}px ${props.spread}px ${props.lightColor}
          , ${props.isInset ? 'inset' : ''} ${props.offset * 2}px ${props.offset * 2}px ${props.spread}px ${props.darkColor}`,
      borderRadius: `${props.borderRadius}px`,
      backgroundColor: props.color,
    },
    '&:active': {
      boxShadow: 'none',
      borderRadius: `${props.borderRadius}px`,
      backgroundColor: props.color,
    },
    '&:focus': {
      boxShadow: `${props.isInset ? 'inset' : ''} -${props.offset * 1.5}px -${props.offset * 1.5}px ${props.spread}px ${props.lightColor}
          , ${props.isInset ? 'inset' : ''} ${props.offset * 1.5}px ${props.offset * 1.5}px ${props.spread}px ${props.darkColor}`,
      borderRadius: `${props.borderRadius}px`,
      backgroundColor: props.color,
    },
  }),
  wrapper: (props) => ({
    position: 'relative',
    borderRadius: `${props.borderRadius}px`,
  }),
  progress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}));

const NeuIconButton = ({
  elevation = 1, textColor, pending, ...otherProps
}) => {
  const theme = useTheme();
  const isInset = elevation < 0;
  const elevationFactor = Math.abs(elevation);
  const offset = elevationFactor * 2;
  const spread = elevationFactor * 6;
  const color = theme.palette[otherProps.color]?.main || theme.palette.background.default;
  const lightColor = new Color(color).lighten(0.8).fade(0.1).rgb()
    .string();
  const darkColor = new Color(color).darken(0.6).fade(0.6).rgb()
    .string();

  const classes = useStyles({
    isInset,
    offset,
    spread,
    lightColor,
    color,
    darkColor,
    textColor: theme.palette[textColor]?.main || textColor?.(theme),
  });

  const Btn = () => (pending ? (
    <div className={classes.wrapper}>
      <IconButton
        classes={{
          root: classes.root,
        }}
        {...otherProps}
        disabled={pending}
        variant="contained"
      />
      <CircularProgress size={24} className={classes.progress} />
    </div>
  ) : (
    <IconButton
      classes={{
        root: classes.root,
      }}
      {...otherProps}
      variant="contained"
    />
  ));

  return (<Btn />);
};

NeuIconButton.propTypes = {
  elevation: PropTypes.number,
  borderRadius: PropTypes.number,
  textColor: PropTypes.any,
  pending: PropTypes.bool,
};

export default NeuIconButton;
