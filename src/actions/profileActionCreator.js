import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  PROFILE_SET_POSTS,
  PROFILE_APPEND_POSTS,
  PROFILE_SET_NEXT_PAGE_URL,
  SAGA_LOAD_PROFILE_POSTS,
  SAGA_PROFILE_POST_LOAD_MORE,
} = ActionTypes;


export const setProfilePosts = createAction(PROFILE_SET_POSTS);
export const appendProfilePosts = createAction(PROFILE_APPEND_POSTS);
export const setProfileNextPageUrl = createAction(PROFILE_SET_NEXT_PAGE_URL);
export const loadProfilePosts = createAction(SAGA_LOAD_PROFILE_POSTS);
export const profilePostsLoadMore = createAction(SAGA_PROFILE_POST_LOAD_MORE);
