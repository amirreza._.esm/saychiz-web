import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  SAGA_CREATE_POST,
  SAGA_DELETE_POST,
  SAGA_GET_USER_POSTS,
  SAGA_LIKE_POST,
  SAGA_UNLIKE_POST,
} = ActionTypes;

export const createPost = createAction(SAGA_CREATE_POST);
export const deletePost = createAction(SAGA_DELETE_POST);
export const getUserPosts = createAction(SAGA_GET_USER_POSTS);
export const likePost = createAction(SAGA_LIKE_POST);
export const unlikePost = createAction(SAGA_UNLIKE_POST);
