import { handleActions } from 'redux-actions';
import {
  setPost, setCommentList, setLikersList, setIsPostLoading, setAreCommentsLoading,
} from '@actions/postDetailActionCreator';

const initialState = {
  postId: null,
  commentList: [],
  likersList: [],
  isPostLoading: false,
  areCommentsLoading: false,
};

export default handleActions({
  [setPost]: (state, { payload }) => ({ ...state, post: payload }),
  [setCommentList]: (state, { payload }) => ({ ...state, commentList: payload }),
  [setLikersList]: (state, { payload }) => ({ ...state, likersList: payload }),
  [setIsPostLoading]: (state, { payload }) => ({ ...state, isPostLoading: payload }),
  [setAreCommentsLoading]: (state, { payload }) => ({ ...state, areCommentsLoading: payload }),
}, initialState);
