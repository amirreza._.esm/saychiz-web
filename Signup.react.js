import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, TextField, Typography } from '@material-ui/core';

const Signup = () => {
  const error = useSelector((state) => state.login.error);

  const [formValues, setFormValues] = useState({
    username: '',
    nickname: '',
    password: '',
    email: '',
    confirmPassword: '',
    phone: '',
    birthday: '',
    gender: '',
    countries: ['Iran', 'North korea', 'Iraq', 'Lebanon', 'Libya', 'Syria', 'Yemen'],
    country: '',
  });

  const handleFormChange = (prop) => (e) => setFormValues(
    { ...formValues, [prop]: e.target.value },
  );

  const handleSubmit = () => { /* sending json to back */

  };

  return (
    <>
      <TextField
        label="Username"
        value={formValues.username}
        onChange={handleFormChange('username')}
        error={!!error}
        required
      />
      <TextField
        label="Nickname"
        value={formValues.nickname}
        onChange={handleFormChange('nickname')}
        error={!!error}
        required
      />
      <TextField
        label="Password"
        type="password"
        value={formValues.password}
        onChange={handleFormChange('password')}
        error={!!error}
        required
      />
      <TextField
        label="Confirm Password"
        type="password"
        value={formValues.confirmPassword}
        onChange={handleFormChange('confirmPassword')}
        error={!!error}
        required
      />
      <TextField
        label="Email"
        placeholder="example@***.***"
        type="email"
        value={formValues.email}
        onChange={handleFormChange('email')}
        error={!!error}
        required
      />
      <TextField
        label="Phone number"
        placeholder="+989131234567"
        type="number"
        value={formValues.phone}
        onChange={handleFormChange('phone')}
        error={!!error}
        required
      />
      <TextField
        label="Date of birth"
        placeholder="yyyy-mm-dd"
        value={formValues.birthday}
        onChange={handleFormChange('birthday')}
        error={!!error}
        required
      />
      <TextField
        label="Country"
        value={formValues.country}
        onChange={handleFormChange('country')}
        error={!!error}
        required
      />
      <TextField
        label="Gender"
        placeholder="1=male,2=female,3=other"
        value={formValues.gender}
        onChange={handleFormChange('gender')}
        error={!!error}
        required
      />
      <Typography
        color={error ? 'error' : 'initial'}
      >
        {error || 'username: hi, password: hello'}
      </Typography>
      <Button
        onClick={handleSubmit}
      >
        Go!
      </Button>
    </>
  );
};

export default Signup;
