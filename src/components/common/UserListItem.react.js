import React from 'react';
import {
  Grid, Avatar, Typography, makeStyles,
} from '@material-ui/core';
import * as PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
  itemBody: {
    cursor: 'pointer',
  },
});

const UserListItem = ({ user }) => {
  const history = useHistory();
  const classes = useStyles();

  const handleItemClick = () => {
    history.push(`/${user.username}`);
  };

  return (
    <Grid
      container
      className={classes.itemBody}
      alignItems="center"
      spacing={2}
      onClick={handleItemClick}
    >
      <Grid item>
        <Avatar src={user.avatar} />
      </Grid>
      <Grid item xs={5} container direction="column">
        <Grid item>
          <Typography variant="body2" color="textPrimary">{`@${user.username}`}</Typography>
        </Grid>
        <Grid item>
          <Typography variant="caption" color="textSecondary">{user.nickname}</Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};

UserListItem.propTypes = {
  user: PropTypes.object.isRequired,
};

export default UserListItem;
