import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Typography, Grid, CircularProgress, InputAdornment, Tooltip, makeStyles, Box,
} from '@material-ui/core';
import BDate from '../custom.signup.components/Date.react';
import * as signupActionCreator from '@actions/signupActionCreator';
import InputAdornments from '../custom.signup.components/Password.react';
import GenderSelect from '../custom.signup.components/Gender.react';
import CountrySelect from '../custom.signup.components/Countries.react';
import NeuTextField from '../neumorphism/NeuTextField.react';
import NeuSection from '../neumorphism/NeuSection.react';
import NeuButton from '../neumorphism/NeuButton.react';
import ThirdPartyAuth from './ThirdPartyAuth.react';
import { stateModes, fieldRegex } from '@constants';
import ErrorIcon from '@material-ui/icons/Error';

const {
  usernameRegex,
  emailRegex,
  onlyNumberRegex,
  phoneNumberRegex,
} = fieldRegex;

const useStyles = makeStyles((theme) => ({
  errorTooltip: {
    backgroundColor: theme.palette.error.dark,
    color: theme.palette.error.contrastText,
    maxWidth: 500,
    fontSize: '0.8725rem',
    whiteSpace: 'pre-line',
  },
}));

const Signup = () => {
  const dispatch = useDispatch();
  const serverError = useSelector((state) => state.signup.serverError);
  const isPending = useSelector((state) => state.signup.isPending);
  const classes = useStyles();

  const isEmailAvailable = useSelector((state) => state.signup.isEmailAvailable);
  const isUsernameAvailable = useSelector((state) => state.signup.isUsernameAvailable);
  const countryList = useSelector((state) => state.signup.countryList);

  const [formValues, setFormValues] = useState({
    username: '',
    email: '',
    nickname: '',
    birthday: '',
    gender: '',
    password: '',
    confirmPassword: '',
    country: '',
    phone: '',
  });

  const [errors, setErrors] = useState({
    username: [],
    email: [],
    nickname: [],
    birthday: [],
    gender: [],
    password: [],
    confirmPassword: [],
    country: [],
    phone: [],
  });


  const createErrorMessage = (errorsObj) => {
    let errorMessage = '';
    errorsObj && Object.keys(errorsObj).forEach((errorType) => {
      if (Array.isArray(errorsObj[errorType])) {
        errorsObj[errorType].forEach((error, index) => {
          if (index === 0) {
            errorMessage += `${errorType}:\n\t${error}\n`;
          } else {
            errorMessage += `${errorType}: ${error}\n`;
          }
        });
      }
    });
    return errorMessage;
  };

  const haveAnyError = (errorsObj) => {
    if (!errorsObj) return false;
    return Object.values(errorsObj).findIndex((value) => value.length > 0) !== -1;
  };
  const haveEmptyField = () => {
    return Object.values(formValues).lastIndexOf('') !== -1;
  };

  const mainErrors = haveAnyError(errors) ? errors : serverError;

  const mainErrorMessage = createErrorMessage(mainErrors);


  useEffect(() => {
    dispatch(signupActionCreator.updateCountryList());
  }, [dispatch]);

  const handleCheckAvailability = (type, value) => {
    switch (type) {
      case 'email':
        dispatch(signupActionCreator.checkEmailAvailable(value));
        break;
      case 'username':
        dispatch(signupActionCreator.checkUsernameAvailable(value));
        break;

      default:
        break;
    }
  };
  const resetErrors = (fieldName) => setErrors((state) => ({ ...state, [fieldName]: [] }));
  const appendToErrors = (fieldName, value) => {
    if (errors[fieldName].lastIndexOf(value) === -1) {
      setErrors((state) => ({ ...state, [fieldName]: [...state[fieldName], value] }));
    }
  };

  // eslint-disable-next-line consistent-return
  const checkForErrors = (fieldName, value) => {
    const errorList = [];
    const cpErrorList = [];
    switch (fieldName) {
      case 'username':
        if (!value) {
          return setErrors((state) => ({ ...state, [fieldName]: [] }));
        }
        resetErrors('username');
        if (!usernameRegex.test(value)) {
          errorList.push('Username should contain Letters, digits and ".","+","-","_" only.');
        }
        if (value.length > 150) {
          errorList.push('Username maximum length is 150.');
        }
        setErrors((state) => ({ ...state, username: errorList }));
        break;
      case 'email':
        if (!value) {
          return setErrors((state) => ({ ...state, [fieldName]: [] }));
        }
        if (!emailRegex.test(value)) {
          errorList.push('Invalid email format.');
        }
        if (value.length > 254) {
          errorList.push('Email maximum length is 254.');
        }
        setErrors((state) => ({ ...state, email: errorList }));
        break;
      case 'password':
        if (!value) {
          setErrors((state) => ({ ...state, confirmPassword: [] }));
          return setErrors((state) => ({ ...state, [fieldName]: [] }));
        }
        if (onlyNumberRegex.test(value)) {
          errorList.push('Password cannot be numerical only.');
        }
        if (value.length < 8) {
          errorList.push('Password should be at least 8 characters.');
        }
        if (!!formValues.confirmPassword && value !== formValues.confirmPassword) {
          cpErrorList.push('Password and confirm password do not match.');
        }
        setErrors((state) => ({ ...state, password: errorList }));
        setErrors((state) => ({ ...state, confirmPassword: cpErrorList }));
        break;
      case 'confirmPassword':
        if (!value) {
          return setErrors((state) => ({ ...state, [fieldName]: [] }));
        }
        if (value !== formValues.password) {
          errorList.push('Password and confirm password do not match.');
        }
        setErrors((state) => ({ ...state, confirmPassword: errorList }));
        break;
      case 'phone':
        if (!value) {
          return setErrors((state) => ({ ...state, [fieldName]: [] }));
        }
        if (!phoneNumberRegex.test(value)) {
          errorList.push('Invalid phone number format.');
        }
        setErrors((state) => ({ ...state, phone: errorList }));
        break;
      case 'nickname':
        if (!value) {
          return setErrors((state) => ({ ...state, [fieldName]: [] }));
        }
        setErrors((state) => ({ ...state, nickname: errorList }));
        break;
      case 'birthday':
        if (!value) {
          return setErrors((state) => ({ ...state, [fieldName]: [] }));
        }
        setErrors((state) => ({ ...state, birthday: errorList }));
        break;
      case 'gender':
        if (!value) {
          return setErrors((state) => ({ ...state, [fieldName]: [] }));
        }
        setErrors((state) => ({ ...state, gender: errorList }));
        break;
      case 'country':
        if (!value) {
          return setErrors((state) => ({ ...state, [fieldName]: [] }));
        }
        setErrors((state) => ({ ...state, country: errorList }));
        break;
      default:
        break;
    }
  };

  const handleFormChange = (fieldName, validateType) => (e) => {
    const { value } = e.target;
    setFormValues(
      { ...formValues, [fieldName]: value },
    );
    if (validateType) handleCheckAvailability(validateType, value);

    checkForErrors(fieldName, value);
  };

  const onBirthdayChange = (date) => {
    setFormValues({ ...formValues, birthday: date });
    checkForErrors('birthday', date);
  };

  const onPasswordChange = (pass) => {
    setFormValues({ ...formValues, password: pass });
    checkForErrors('password', pass);
  };

  const onConfirmPasswordChange = (confirmPassword) => {
    setFormValues((prevState) => ({ ...prevState, confirmPassword }));
    checkForErrors('confirmPassword', confirmPassword);
  };

  const onGenderChange = (sex) => {
    setFormValues({ ...formValues, gender: sex });
    checkForErrors('gender', sex);
  };

  const onCountryChange = (Country) => {
    setFormValues({ ...formValues, country: Country });
    checkForErrors('country', Country);
  };


  const handleSubmit = () => {
    // eslint-disable-next-line no-unused-vars
    Object.entries(formValues).filter(([_, value]) => value === '').forEach(([name]) => {
      appendToErrors(name, `${name} cannot be empty.`);
    });
    dispatch(signupActionCreator.onSignup(formValues));
  };

  return (
    <Grid
      container
      spacing={2}
      onKeyPress={(e) => {
        if (e.key === 'Enter' && !(haveAnyError(errors) || haveEmptyField())) handleSubmit();
      }}
    >
      <Grid item container justify="center">
        <ThirdPartyAuth />
      </Grid>
      <Grid item container justify="center">
        <Typography color="textSecondary"> or with your email</Typography>
      </Grid>
      <Grid item xs={6}>
        <NeuSection title="Account" fullWidth>
          <NeuTextField
            fullWidth
            id="username"
            label="Username"
            margin="dense"
            value={formValues.username}
            onChange={handleFormChange('username', 'username')}
            error={isUsernameAvailable === stateModes.FAIL || !!errors.username.length}
            required
            {
              ...(isUsernameAvailable === stateModes.PENDING
              && {
                InputProps: {
                  endAdornment: (<InputAdornment><CircularProgress size={24} /></InputAdornment>),
                },
              })
            }
          />
          <NeuTextField
            fullWidth
            id="nickname"
            label="Nickname"
            margin="dense"
            value={formValues.nickname}
            onChange={handleFormChange('nickname')}
            error={!!errors.nickname.length}
            required
          />
        </NeuSection>
      </Grid>
      <Grid item xs={6}>
        <NeuSection title="Contact info">
          <NeuTextField
            fullWidth
            id="email"
            label="Email"
            margin="dense"
            placeholder="example@***.***"
            type="email"
            value={formValues.email}
            onChange={handleFormChange('email', 'email')}
            error={isEmailAvailable === stateModes.FAIL || !!errors.email.length}
            required
            {
              ...(isEmailAvailable === stateModes.PENDING
              && {
                InputProps: {
                  endAdornment: (<InputAdornment><CircularProgress size={24} /></InputAdornment>),
                },
              })
            }
          />
          <NeuTextField
            fullWidth
            id="phobe"
            label="Phone number"
            placeholder="+989131234567"
            margin="dense"
            type="number"
            value={formValues.phone}
            onChange={handleFormChange('phone')}
            error={!!errors.phone.length}
            required
          />
        </NeuSection>
      </Grid>
      <Grid item xs={6}>
        <NeuSection title="Password">
          <InputAdornments
            id="password"
            lid="Password"
            callbackFromParent={onPasswordChange}
            value={formValues.password}
            error={!!errors.password.length}
            required
          />
          <InputAdornments
            id="confirmPassword"
            lid="Confirm Password"
            callbackFromParent={onConfirmPasswordChange}
            value={formValues.confirmPassword}
            error={!!errors.confirmPassword.length}
            required
          />
        </NeuSection>
      </Grid>
      <Grid item xs={6}>
        <NeuSection title="Personal info">
          <BDate
            id="birthday"
            onChange={onBirthdayChange}
            error={!!errors.birthday.length}
          />
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <CountrySelect
                id="country"
                lid="Country"
                callbackFromParent={onCountryChange}
                value={formValues.country}
                error={!!errors.country.length}
                required
                countryList={countryList}
              />
            </Grid>
            <Grid item xs={6}>
              <GenderSelect
                id="gender"
                lid="Gender"
                callbackFromParent={onGenderChange}
                value={formValues.gender}
                error={!!errors.gender.length}
                required
              />
            </Grid>
          </Grid>
        </NeuSection>
      </Grid>

      <Grid item xs={4} container alignItems="center">
        {haveAnyError(mainErrors) && (
        <Tooltip

          PopperProps={{
            disablePortal: true,
          }}
          title={mainErrorMessage}
          classes={{ tooltip: classes.errorTooltip }}
          placement="top"
          arrow
        >

          <Box display="flex" alignItems="center">
            <ErrorIcon color="error" />
            <Typography variant="caption" color="error">
              {Object.values(mainErrors).filter((l) => l.length !== 0).length}
              {' '}
              errors
            </Typography>
          </Box>
        </Tooltip>
        )}
      </Grid>
      <Grid item xs container justify="center">
        <NeuButton
          pending={isPending}
          textColor="primary"
          onClick={() => handleSubmit()}
          disabled={haveAnyError(errors) || haveEmptyField()}
        >
          Sign up
        </NeuButton>
      </Grid>
      <Grid item xs={4} />
    </Grid>
  );
};

export default Signup;
