import { handleActions } from 'redux-actions';
import {
  setIsSearchOpen, setSearchQuery, setSearchResults,
  setIsSearchLoading, setSearchNoResults, appendSearchResults, setSearchNextPageUrl,
} from '@actions/searchActionCreator';

const initialState = {
  isOpen: false,
  isLoading: false,
  noResults: false,
  searchQuery: '',
  searchResults: [],
  nextPageUrl: null,
};

export default handleActions({
  [setIsSearchOpen]: (state, { payload }) => ({ ...state, isOpen: payload }),
  [setIsSearchLoading]: (state, { payload }) => ({ ...state, isLoading: payload }),
  [setSearchNoResults]: (state, { payload }) => ({ ...state, noResults: payload }),
  [setSearchQuery]: (state, { payload }) => ({ ...state, searchQuery: payload }),
  [setSearchResults]: (state, { payload }) => ({ ...state, searchResults: payload }),
  [appendSearchResults]: (state, { payload }) => ({
    ...state,
    searchResults: [...state.searchResults, ...payload],
  }),
  [setSearchNextPageUrl]: (state, { payload }) => ({ ...state, nextPageUrl: payload }),
}, initialState);
