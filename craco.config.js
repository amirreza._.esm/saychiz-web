// eslint-disable-next-line import/no-extraneous-dependencies
const CracoAlias = require('craco-alias');
// eslint-disable-next-line no-unused-vars
module.exports = ({ env }) => ({
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        source: 'jsconfig',
        baseUrl: './',
      },
    },
  ],
});
