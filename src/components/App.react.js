import React, { useEffect } from 'react';
import {
  createMuiTheme, ThemeProvider, makeStyles, CircularProgress,
} from '@material-ui/core';
import { useSelector, Provider, useDispatch } from 'react-redux';
import {
  BrowserRouter, Switch, Route, Redirect,
} from 'react-router-dom';
import AppEntry from './appEntry/AppEntry.react';
import store from '../store';
import Modals from './modals/Modals.react';
import { backgroundGradient } from '../styles/common/gradient';
import { onLogin } from '@actions/loginActionCreator';
import Main from './Main.react';


const useStyles = makeStyles((theme) => ({
  appBody: {
    height: '100%',
    backgroundColor: theme.palette.background.default,
    overflow: 'hidden',
  },
  backgroundGradient: backgroundGradient(theme),
  loadingWrapper: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      // main: '#5BC9A4',
      main: '#FF1185',
    },
    secondary: {
      // main: '#c96d5b',
      main: '#8A85FF',
      // main: '#FF5520',
    },
    text: {
      primary: 'rgba(255, 255, 255, 0.77)',
      secondary: 'rgba(255, 255, 255, 0.43)',
    },
    background: {
      // default: '#CAD1D8',
      // default: '#ECEEEE',
      default: '#20282B',
      paper: '#282f2f',
    },
  },
  typography: {
    fontFamily:
      'Dosis, "Roboto", "Helvetica Neue", Helvetica, Arial, Segoe, sans-serif',
  },
  shape: {
    borderRadius: 5,
  },
});

const AppBody = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const isInitiated = useSelector((state) => state.appState.isInitiated);
  const isLoggedIn = useSelector((state) => state.appState.isLoggedIn);
  const isUserVerified = useSelector((state) => state.appState.isUserVerified);

  useEffect(() => {
    dispatch(onLogin());
  }, [dispatch]);

  return (
    isInitiated ? (
      <div className={`${classes.appBody} ${classes.backgroundGradient}`}>
        <Switch>
          <Route path="/login" render={!isLoggedIn || (isLoggedIn && !isUserVerified) ? () => <AppEntry login /> : () => <Redirect to="/home" />} />
          <Route path="/signup" render={!isLoggedIn || (isLoggedIn && !isUserVerified) ? () => <AppEntry signup /> : () => <Redirect to="/home" />} />
          <Route path="/" render={isLoggedIn ? () => <Main /> : () => <Redirect to="/login" />} />
          {/* <Redirect from="/" to="/app" /> */}
        </Switch>
        <Modals />
      </div>
    ) : (
      <div className={`${classes.loadingWrapper} ${classes.backgroundGradient}`}>
        <CircularProgress />
      </div>
    )
  );
};

const App = () => (
  <ThemeProvider theme={theme}>
    <BrowserRouter>
      <AppBody />
    </BrowserRouter>
  </ThemeProvider>
);

export default () => (
  <Provider store={store}>
    <App />
  </Provider>
);
