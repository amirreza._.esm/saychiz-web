import React, { useEffect } from 'react';
import Login from './Login.react';
import Signup from './Signup.react';
import {
  makeStyles, useTheme, Button, Typography, Box, Grid,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { bool } from 'prop-types';
import Surface from '../common/Surface.react';
import { motion } from 'framer-motion';

import LockOpenTwoToneIcon from '@material-ui/icons/LockOpenTwoTone';
import AccountCircleTwoToneIcon from '@material-ui/icons/AccountCircleTwoTone';

import bg from '@assets/photos/bgPurple.jpg';
import { useSelector, useDispatch } from 'react-redux';
import { openVerificationModal, closeVerificationModal } from '@actions/verificationActionCreator';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
  photoBg: (props) => ({
    backgroundImage: `url("${bg}")`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    borderRadius: theme.shape.borderRadius * props.borderRadius,
  }),
  wrapper: {
    display: 'flex',
    minHeight: 600,
  },
  loginSection: (props) => ({
    // ),
    padding: 10,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopLeftRadius: theme.shape.borderRadius * props.borderRadius,
    borderBottomLeftRadius: theme.shape.borderRadius * props.borderRadius,
  }),
  signupSection: (props) => ({
    padding: 10,
    borderTopRightRadius: theme.shape.borderRadius * props.borderRadius,
    borderBottomRightRadius: theme.shape.borderRadius * props.borderRadius,
  }),
}));

const loginVariants = (theme) => ({
  open: {
    flexBasis: '67%',
    backgroundColor: `${theme.palette.background.default}ff`,
  },
  collapsed: {
    flexBasis: '33%',
    backgroundColor: 'rgba(100,100,100,0)',
  },
});

const signupVariants = (theme) => ({
  open: {
    flexBasis: '67%',
    flexGrow: '1',
    backgroundColor: `${theme.palette.background.default}ff`,
  },
  collapsed: {
    flexBasis: '33%',
    flexGrow: '1',
    backgroundColor: 'rgba(100,100,100,0)',
  },
});


const AppEntry = ({ login, signup }) => {
  const { pathname } = new URL(window.location.href);
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => state.appState.isLoggedIn);
  const isUserVerified = useSelector((state) => state.appState.isUserVerified);

  const borderRadius = 2;
  const history = useHistory();
  const classes = useStyles({ login, signup, borderRadius });
  const theme = useTheme();

  useEffect(() => {
    if (isLoggedIn && !isUserVerified) {
      dispatch(openVerificationModal());
    }
    return (() => dispatch(closeVerificationModal()));
  }, [dispatch, isLoggedIn, isUserVerified]);

  const Banner = ({
    // eslint-disable-next-line react/prop-types
    IconComponent, title, body, handler,
  }) => (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
      height="100%"
      width="100%"
    >
      <Grid container spacing={6}>
        <Grid item container justify="center">
          <Box
            fontFamily="Satisfy !important"
            color="background.default"
            clone
          >
            <Typography variant="h3">
              SayChiz
            </Typography>
          </Box>
        </Grid>
        <Grid item container justify="center" spacing={2}>
          <Grid item container justify="center">
            <Box fontSize="5em" color="background.default" clone>
              <IconComponent />
            </Box>
          </Grid>
          <Grid item container direction="column" alignItems="center">
            <Box color="background.default" textAlign="center" fontWeight="500 !important" clone>
              <Typography variant="h4">{title}</Typography>
            </Box>
            <br />
            <Box color="background.default" textAlign="center" clone maxWidth={300}>
              <Typography variant="body2">{body}</Typography>
            </Box>
          </Grid>
        </Grid>
        <Grid item container justify="center">
          <Box color="background.default">
            <Button variant="outlined" color="inherit" onClick={handler}>{pathname === '/login' ? 'Go to Signup' : 'Go to Login'}</Button>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );

  return (
    <Grid
      className={classes.root}
      container
      justify="center"
    >
      <Box
        minWidth={{ xs: 600 }}
        minHeight={600}
        maxWidth={1200}
        mt={10}
      >
        <Surface
          startUp
          elevation={2}
          borderRadius={borderRadius}
        >
          <div className={classes.photoBg}>
            <div className={classes.wrapper}>
              <motion.div
                className={classes.loginSection}
                initial={false}
                animate={login ? 'open' : 'collapsed'}
                variants={loginVariants(theme)}
                transition={{ type: 'tween' }}
              >
                {
          login
            ? (
              <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                flexDirection="column"
                maxWidth={500}
                mx="auto"
                height="100%"
              >
                <Box mb={4}>
                  <Box fontWeight="600 !important" clone>
                    <Typography variant="h3" color="primary">Sign in to Say Chiz</Typography>
                  </Box>
                </Box>
                <Login />
              </Box>
            )
            : (
              <Banner
                IconComponent={AccountCircleTwoToneIcon}
                title="Hello there!"
                body="To create an account, fill in some info about yourself and you&apos;re done!"
                handler={() => history.push('/login')}
              />
            )
          }
              </motion.div>
              <motion.div
                className={classes.signupSection}
                initial={false}
                animate={signup ? 'open' : 'collapsed'}
                variants={signupVariants(theme)}
                transition={{ type: 'tween', duration: 0.2 }}
              >
                {
          signup
            ? (
              <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                flexDirection="column"
                maxWidth={500}
                mx="auto"
                height="100%"
              >
                <Box mb={4}>
                  <Box fontWeight="600 !important" clone>
                    <Typography variant="h3" color="primary">Create Account</Typography>
                  </Box>
                </Box>
                <Signup />
              </Box>
            )
            : (
              <Banner
                IconComponent={LockOpenTwoToneIcon}
                title="Welcome Back!"
                body="Continue where you left off!"
                handler={() => history.push('/signup')}
              />
            )
          }
              </motion.div>
            </div>
          </div>
        </Surface>
      </Box>
    </Grid>
  );
};

AppEntry.propTypes = {
  login: bool,
  signup: bool,
};

export default AppEntry;
