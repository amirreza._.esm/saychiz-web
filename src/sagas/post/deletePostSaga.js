import { takeEvery, call, put } from 'redux-saga/effects';
import { deletePost } from '@actions/postActionCreator';
import * as Api from '@api';
import { loadTimelinePosts } from '@actions/timeLineActionCreator';

// not finished
function* worker({ payload }) {
  try {
    const { postId } = payload;
    yield call(Api.deletePost, postId);
    yield put(loadTimelinePosts());
  } catch (e) {
    console.log(e);
  }
}

export default function* deletePostSaga() {
  yield takeEvery(deletePost, worker);
}
