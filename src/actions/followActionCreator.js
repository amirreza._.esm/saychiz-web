import { ActionTypes } from '../constants/AppConstants';
import { createAction } from 'redux-actions';

const {
  SAGA_FOLLOW_FOLLOW_USER,
  SAGA_FOLLOW_UNFOLLOW_USER,

} = ActionTypes;


export const followUser = createAction(SAGA_FOLLOW_FOLLOW_USER);
export const unfollowUser = createAction(SAGA_FOLLOW_UNFOLLOW_USER);
