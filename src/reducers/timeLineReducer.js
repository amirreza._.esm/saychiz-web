import { handleActions } from 'redux-actions';
import { setTimelinePosts, setTimelineNextPageUrl, appendTimelinePosts } from '@actions/timeLineActionCreator';

const initialState = {
  posts: [],
  nextPageUrl: null,
};

export default handleActions({
  [setTimelinePosts]: (state, { payload }) => ({ ...state, posts: payload }),
  [appendTimelinePosts]: (state, { payload }) => ({
    ...state,
    posts: [...state.posts, ...payload],
  }),
  [setTimelineNextPageUrl]: (state, { payload }) => ({ ...state, nextPageUrl: payload }),
}, initialState);
