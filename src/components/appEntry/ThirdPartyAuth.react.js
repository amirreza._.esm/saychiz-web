import React from 'react';
import {
  Box, Grid, IconButton,
} from '@material-ui/core';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';


const ThirdPartyAuth = () => {
  return (
    <Box>
      <Grid container spacing={1}>
        <Grid item>
          <IconButton>
            <InstagramIcon color="secondary" />
          </IconButton>
        </Grid>
        <Grid item>
          <IconButton>
            <TwitterIcon color="secondary" />
          </IconButton>
        </Grid>
        <Grid item>
          <IconButton>
            <FacebookIcon color="secondary" />
          </IconButton>
        </Grid>
        <Grid item>
          <IconButton>
            <LinkedInIcon color="secondary" />
          </IconButton>
        </Grid>
      </Grid>
    </Box>
  );
};
export default ThirdPartyAuth;
