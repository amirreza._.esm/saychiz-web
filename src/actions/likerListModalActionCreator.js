import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  LIKER_LIST_MODAL_OPEN,
  LIKER_LIST_MODAL_CLOSE,
  LIKER_LIST_MODAL_SET_LIKER_LIST,
} = ActionTypes;

export const openLikerListModal = createAction(LIKER_LIST_MODAL_OPEN);
export const closeLikerListModal = createAction(LIKER_LIST_MODAL_CLOSE);

export const setLikerList = createAction(LIKER_LIST_MODAL_SET_LIKER_LIST);
