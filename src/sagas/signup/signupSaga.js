import {
  put, call, takeEvery,
} from 'redux-saga/effects';
import { setSignupServerError, onSignup, setSignupPending } from '../../actions/signupActionCreator';
import * as Api from '@api';
import { setAuthToken, setCurrentUser } from '@actions/currentUserActionCreator';
import { setIsLoggedIn, setIsUserVerified } from '@actions/appStateActionCreator';
import { setVerificationCode } from '@actions/verificationActionCreator';


function* signupWorkerSaga({ payload }) {
  const {
    username, email, gender, phone, birthday, country, nickname, password,
  } = payload;
  yield put(setSignupServerError(null));
  yield put(setSignupPending(true));
  try {
    const results = yield call(
      Api.signup, username, email, gender, phone, birthday, country, nickname, password,
    );
    const { user, token, verify_code: verifyCode } = results.data;
    yield put(setAuthToken(token));
    yield put(setCurrentUser(user));
    yield call(Api.storeToken, token);
    yield put(setIsLoggedIn(true));
    if (verifyCode !== 0) {
      yield put(setIsUserVerified(false));
      yield put(setVerificationCode(verifyCode));
    } else {
      yield put(setIsUserVerified(true));
    }
  } catch (e) {
    yield put(setSignupServerError(e.data));
  } finally {
    yield put(setSignupPending(false));
  }
}

export default function* signupSaga() {
  yield takeEvery(onSignup, signupWorkerSaga);
}
