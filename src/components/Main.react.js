import React from 'react';
import {
  Grid, Box,
} from '@material-ui/core';
import Home from './Home.react';
import Menu from './Menu.react';
import ToolBar from './ToolBar.react';
import { Switch, Route, Redirect } from 'react-router-dom';
import PostDetail from './postComponents/PostDetail.react';
import UserProfile from './appEntry/userProfile/UserProfile.react';
import UserFollowers from './appEntry/userProfile/UserFollowers.react';
import UserFollowing from './appEntry/userProfile/UserFollowing.react';


const Main = () => {
  const renderMainContent = () => (
    <Switch>
      <Route path="/home">
        <Home />
      </Route>
      <Route path="/:username/posts/:postId">
        <PostDetail />
      </Route>
      <Route path="/:username/followers">
        <UserFollowers />
      </Route>
      <Route path="/:username/following">
        <UserFollowing />
      </Route>
      <Route path="/:username">
        <UserProfile />
      </Route>
      <Redirect to="/home" />
    </Switch>
  );
  return (
    <Box px={3} clone>
      <Grid container spacing={3}>
        <Grid item xs={2}>
          <Menu />
        </Grid>
        <Grid item xs>
          {renderMainContent()}
        </Grid>
        <Grid item xs={3}>
          <ToolBar />
        </Grid>
      </Grid>
    </Box>
  );
};

export default Main;
