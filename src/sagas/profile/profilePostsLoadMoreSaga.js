import {
  call, select, put, takeLeading,
} from 'redux-saga/effects';
import * as Api from '@api';
import { profilePostsLoadMore, appendProfilePosts, setProfileNextPageUrl } from '@actions/profileActionCreator';

function* profilePostsLoadMoreWorkerSaga() {
  const nextPageUrl = yield select((state) => state.timeLine.nextPageUrl);
  try {
    if (nextPageUrl) {
      const {
        data: {
        // eslint-disable-next-line no-unused-vars
          count, next, previous, results,
        },
      } = yield call(Api.timelineLoadMore, nextPageUrl);

      yield put(appendProfilePosts(results));
      yield put(setProfileNextPageUrl(next));
    }
  } catch (e) {
    console.log(e);
  }
}

export default function* profilePostsLoadMoreSaga() {
  yield takeLeading(profilePostsLoadMore, profilePostsLoadMoreWorkerSaga);
}
