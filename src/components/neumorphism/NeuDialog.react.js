import React from 'react';
import { Dialog, makeStyles } from '@material-ui/core';
import Color from 'color';
import * as PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  paper: (props) => ({
    backgroundColor: props.opacityLevel === 'light'
      ? 'transparent'
      : Color(theme.palette.background.default).fade(0.15).toString(),
    borderRadius: theme.shape.borderRadius * props.borderRadius,
  }),
  backDrop: {
    backgroundColor: '#00000040',
  },
}));

const NeuDialog = ({ borderRadius = 2, opacityLevel = 'light', ...otherProps }) => {
  const classes = useStyles({ borderRadius, opacityLevel });
  return (
    <Dialog
      PaperProps={{ className: classes.paper }}
      BackdropProps={{ className: classes.backDrop }}
      {...otherProps}
    />
  );
};

NeuDialog.propTypes = {
  borderRadius: PropTypes.number,
  opacityLevel: PropTypes.oneOf(['light', 'dark']),
};

export default NeuDialog;
