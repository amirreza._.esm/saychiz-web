import React from 'react';
import { makeStyles, useTheme, Typography } from '@material-ui/core';
import Color from 'color';

const useStyles = makeStyles(() => ({
  wrapper: (props) => ({
    ...(props.fullWidth && { width: '100%' }),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginBottom: 10,
  }),
  header: {
    alignSelf: 'stretch',
    display: 'flex',
    alignItems: 'center',
  },
  title: {
    marginLeft: 5,
    marginRight: 5,
  },
  dividerStart: (props) => ({
    height: 8,
    flexBasis: '10%',
    boxShadow: `inset -${props.offset}px -${props.offset}px ${props.spread}px ${props.lightColor}
        ,inset  ${props.offset}px ${props.offset}px ${props.spread}px ${props.darkColor}`,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  }),
  dividerEnd: (props) => ({
    height: 8,
    flexGrow: 1,
    boxShadow: `inset -${props.offset}px -${props.offset}px ${props.spread}px ${props.lightColor}
        ,inset  ${props.offset}px ${props.offset}px ${props.spread}px ${props.darkColor}`,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  }),
}));

const NeuSection = ({
  title, fullWidth, elevation = 1, children,
}) => {
  const theme = useTheme();
  const offset = elevation * 2;
  const spread = elevation * 6;
  const color = theme.palette.background.default;
  const lightColor = new Color(color).lighten(0.6).fade(0.1).rgb()
    .string();
  const darkColor = new Color(color).darken(0.6).fade(0.6).rgb()
    .string();

  const classes = useStyles({
    offset,
    spread,
    lightColor,
    color,
    darkColor,
    fullWidth,
  });
  return (
    <div className={classes.wrapper}>
      <div className={classes.header}>
        <span className={classes.dividerStart} />
        <Typography variant="caption" color="textSecondary" className={classes.title}>{title}</Typography>
        <span className={classes.dividerEnd} />
      </div>
      {children}
    </div>
  );
};

export default NeuSection;
