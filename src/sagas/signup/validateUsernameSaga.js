import {
  put, call, takeLatest, delay,
} from 'redux-saga/effects';
import * as Api from '@api';
import {
  checkUsernameAvailable, setUsernameAvailable,
} from '@actions/signupActionCreator';
import { stateModes } from '@constants';

function* worker({ payload }) {
  yield delay(300);
  yield put(setUsernameAvailable(stateModes.PENDING));
  try {
    yield call(Api.validateUsername, payload);
    yield put(setUsernameAvailable(stateModes.SUCCESS));
  } catch (e) {
    yield put(setUsernameAvailable(stateModes.FAIL));
  }
}

export default function* validateUsernameSaga() {
  yield takeLatest(checkUsernameAvailable, worker);
}
