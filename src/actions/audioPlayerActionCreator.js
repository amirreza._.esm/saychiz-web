import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  AUDIO_PLAYER_SET_URL,
  AUDIO_PLAYER_SET_POST_ID,
  AUDIO_PLAYER_SET_IS_PLAYING,
  AUDIO_PLAYER_SET_CURRENT_TIME_HANDLER,
  AUDIO_PLAYER_SEEK_CURRENT_TIME,
} = ActionTypes;

export const setUrl = createAction(AUDIO_PLAYER_SET_URL);
export const setPlayerPostId = createAction(AUDIO_PLAYER_SET_POST_ID);
export const setPlayerIsPlaying = createAction(AUDIO_PLAYER_SET_IS_PLAYING);
export const setPlayerCurrentTimeHandler = createAction(AUDIO_PLAYER_SET_CURRENT_TIME_HANDLER);
export const setPlayerSeekCurrentTime = createAction(AUDIO_PLAYER_SEEK_CURRENT_TIME);
