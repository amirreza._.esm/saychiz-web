import React, { useState } from 'react';
import moment from 'moment';
import {
  Box, Grid, Avatar, Typography, makeStyles, Menu, MenuItem,
} from '@material-ui/core';
import Surface from '../common/Surface.react';
import NeuIconButton from '../neumorphism/NeuIconButton.react';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import CommentIcon from '@material-ui/icons/Comment';
import DeleteIcon from '@material-ui/icons/Delete';
import NeuButton from '../neumorphism/NeuButton.react';
import * as PropTypes from 'prop-types';
import WaveFormVisualizer from './WaveFormVisualizer.react';
import { useSelector, useDispatch } from 'react-redux';
import { setPlayerPostId, setPlayerIsPlaying, setUrl } from '@actions/audioPlayerActionCreator';
import { unlikePost, likePost, deletePost } from '@actions/postActionCreator';
import { openReplyModal } from '@actions/ReplyToPostActionCreator';
import { useHistory } from 'react-router-dom';
import { setPost } from '@actions/postDetailActionCreator';
import { motion } from 'framer-motion';

const useStyles = makeStyles((theme) => ({
  rootWrapper: ({ clickable }) => ({
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(3),
    paddingBottom: theme.spacing(1),
    ...(clickable && {
      filter: 'brightness(1.0)',
      '&:hover': {
        cursor: 'pointer',
      },
    }),
  }),
  avatarWrapper: {
  },
  voiceWrapper: {
    marginTop: theme.spacing(1),
  },
  captionWrapper: {
    marginTop: theme.spacing(2),
  },
  actionsWrapper: {
    marginTop: theme.spacing(2),
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },

  visualizer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end',
    height: theme.spacing(4),
    width: '100%',
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
  },
  playBtn: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
}));

const postBodyVariants = {
  hover: {
    filter: 'brightness(1.2)',
  },
};


const Post = ({ post, clickable }) => {
  const classes = useStyles({ clickable });
  const dispatch = useDispatch();
  const history = useHistory();

  const currentUser = useSelector((state) => state.currentUser.currentUser);
  const playerPostId = useSelector((state) => state.audioPlayer.postId);
  const playerIsPlaying = useSelector((state) => state.audioPlayer.isPlaying);


  const [menuAnchorEl, setMenuAnchorEl] = useState(null);

  const {
    id, author, caption, createdDate, countLike, countComment, isLiked,
    audioFile: { waveform, duration },
  } = post;

  const isPlaying = id === playerPostId && playerIsPlaying;
  const showSelfPostSetting = currentUser.id === author.id;

  const handleShowPostDetail = () => {
    dispatch(setPost(post));
    history.push(`/${author.username}/posts/${id}/`);
  };

  const handlePlayToggle = (e) => {
    e.stopPropagation();
    if (isPlaying) {
      dispatch(setPlayerIsPlaying(false));
    } else {
      dispatch(setUrl(''));
      dispatch(setPlayerIsPlaying(true));
      dispatch(setPlayerPostId(id));
    }
  };

  const handleLike = (e) => {
    e.stopPropagation();
    if (isLiked) {
      dispatch(unlikePost({ postId: id }));
    } else {
      dispatch(likePost({ postId: id, authorId: currentUser.id }));
    }
  };

  const handleReply = (e) => {
    e.stopPropagation();
    dispatch(openReplyModal({ sourcePostId: id, sourceAuthor: author }));
  };

  const handleDelete = (e) => {
    e.stopPropagation();
    dispatch(deletePost({ postId: id }));
    setMenuAnchorEl(null);
  };

  const relativeCreationDate = moment(createdDate).fromNow();

  // more button options
  const menuItemList = [];
  showSelfPostSetting && menuItemList.push(
    <MenuItem onClick={handleDelete}>
      <DeleteIcon />
      &nbsp;
      Delete
    </MenuItem>,
  );

  return (
    <motion.div
      className={classes.rootWrapper}
      {
        ...(clickable && {
          variants: postBodyVariants,
          whileHover: 'hover',
          onClick: handleShowPostDetail,
        })
      }
    >
      <Grid container className={classes.avatarWrapper}>
        <Grid item xs container justify="flex-start" alignItems="center">
          <Avatar
            alt={author?.nickname}
            src={author?.avatar}
            className={classes.avatar}
          />
        </Grid>
        <Grid item xs={1} container justify="center" alignItems="flex-start">
          {
          menuItemList.length > 0 && (
          <NeuIconButton
            elevation={0}
            textColor={(theme) => theme.palette.text.secondary}
            size="medium"
            onClick={(e) => {
              e.stopPropagation();
              setMenuAnchorEl({
                top: e.target.getBoundingClientRect().top,
                left: e.target.getBoundingClientRect().left,
              });
            }}
          >
            <MoreVertIcon />
          </NeuIconButton>
          )
}
        </Grid>
      </Grid>
      <Grid container className={classes.voiceWrapper}>
        <Grid item xs={2} container alignItems="center">
          <Grid item xs={12}>
            <Typography variant="body2" color="textPrimary">{author?.nickname}</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="caption" color="textSecondary">{`@${author?.username}`}</Typography>
          </Grid>
        </Grid>
        <Grid item xs container justify="center" alignItems="center">
          <Surface borderRadius={1} elevation={0.5} className={classes.visualizer}>

            <WaveFormVisualizer
              waveForm={waveform}
              isPlaying={isPlaying}
              duration={duration}
            />
          </Surface>
        </Grid>
        <Grid item xs={1} container justify="center" alignItems="center">
          <NeuIconButton
            className={classes.playBtn}
            elevation={0.5}
            textColor="primary"
            onClick={handlePlayToggle}
          >
            {isPlaying ? <PauseIcon /> : <PlayArrowIcon />}
          </NeuIconButton>
        </Grid>
      </Grid>
      <Grid container className={classes.captionWrapper}>
        <Grid item xs={2} />
        <Grid item xs>
          <Typography
            variant="body2"
            color="textSecondary"
          >
            {caption}
          </Typography>
        </Grid>
        <Grid item xs={1} />
      </Grid>
      <Grid container className={classes.actionsWrapper}>
        <Grid item xs={9} container alignItems="center">
          <Box>
            <Typography color="textSecondary" variant="caption">
              {relativeCreationDate}
            </Typography>
          </Box>
        </Grid>
        <Grid item xs container justify="space-evenly" alignItems="center">
          <NeuButton
            elevation={0}
            // hoverElevation={0.5}
            onClick={handleLike}
            textColor={
              (theme) => (isLiked ? theme.palette.primary.main : theme.palette.text.primary)
            }
            hoverTextColor="primary"
          >
            <FavoriteBorderIcon />
            &nbsp;
            {countLike}
          </NeuButton>
          <NeuButton
            elevation={0}
            // hoverElevation={0.5}
            onClick={handleReply}
            textColor={(theme) => theme.palette.text.primary}
            hoverTextColor="secondary"
          >
            <CommentIcon />
            &nbsp;
            {countComment}
          </NeuButton>
        </Grid>
      </Grid>
      {
      menuItemList.length > 0 && (
      <Menu
        open={!!menuAnchorEl}
        anchorReference="anchorPosition"
        anchorPosition={menuAnchorEl}
        onClose={() => setMenuAnchorEl(null)}
      >
        {menuItemList}
      </Menu>
      )
}
    </motion.div>
  );
};

Post.propTypes = {
  post: PropTypes.object,
  clickable: PropTypes.bool,
};

export default Post;
