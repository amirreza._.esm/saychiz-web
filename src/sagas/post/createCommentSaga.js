import { takeLatest, call } from 'redux-saga/effects';
import * as Api from '@api';
import { createComment } from '@actions/ReplyToPostActionCreator';

function* worker({ payload }) {
  try {
    const {
      sourcePostId, authorId, caption, file, fileName, fileType, duration,
    } = payload;
    yield call(Api.createComment, sourcePostId, authorId, caption, file, fileName, fileType, duration);
  } catch (e) {

  }
}

export default function* createCommentSaga() {
  yield takeLatest(createComment, worker);
}
