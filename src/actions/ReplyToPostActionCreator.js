import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  REPLY_MODAL_OPEN,
  REPLY_MODAL_CLOSE,
  SAGA_CREATE_COMMENT,
} = ActionTypes;

export const openReplyModal = createAction(REPLY_MODAL_OPEN);
export const closeReplyModal = createAction(REPLY_MODAL_CLOSE);

export const createComment = createAction(SAGA_CREATE_COMMENT);
