import { handleActions } from 'redux-actions';
import { openVerificationModal, closeVerificationModal } from '@actions/verificationActionCreator';

const initialState = {
  verificationIsOpen: false,
  followIsOpen: false,
};

export default handleActions({
  [openVerificationModal]: (state) => ({ ...state, verificationIsOpen: true }),
  [closeVerificationModal]: (state) => ({ ...state, verificationIsOpen: false }),
}, initialState);
