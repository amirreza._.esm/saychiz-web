import { combineReducers } from 'redux';
import appStateReducer from './appStateReducer';
import currentUserReducer from './currentUserReducer';
import loginReducer from './loginReducer';
import signupReducer from './signupReducer';
import modalsReducer from './modalsReducer';
import verificationReducer from './verificationReducer';
import visitedUserReducer from './visitedUserReducer';
import timeLineReducer from './timeLineReducer';
import audioPlayerReducer from './audioPlayerReducer';
import ReplyToPostReducer from './ReplyToPostReducer';
import postDetailReducer from './postDetailReducer';
import likerListModalReducer from './likerListModalReducer';
import searchReducer from './searchReducer';
import profileReducer from './profileReducer';

export default combineReducers({
  appState: appStateReducer,
  modals: modalsReducer,
  currentUser: currentUserReducer,
  login: loginReducer,
  signup: signupReducer,
  verification: verificationReducer,
  visitedUser: visitedUserReducer,
  timeLine: timeLineReducer,
  audioPlayer: audioPlayerReducer,
  replyToPost: ReplyToPostReducer,
  postDetail: postDetailReducer,
  likerListModal: likerListModalReducer,
  search: searchReducer,
  profile: profileReducer,
});
