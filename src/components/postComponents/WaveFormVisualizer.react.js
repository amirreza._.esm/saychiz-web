import React, { useRef, useEffect, useState } from 'react';
import { useTheme, makeStyles } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { setPlayerCurrentTimeHandler, setPlayerSeekCurrentTime } from '@actions/audioPlayerActionCreator';

const useStyles = makeStyles({
  canvas: {
    cursor: 'pointer',
  },
});

const WaveFormVisualzer = ({
  waveForm, isPlaying, duration,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const canvasRef = useRef();
  const dispatch = useDispatch();

  const [currentTime, setCurrentTime] = useState(0);

  const percent = currentTime && duration && (currentTime / duration);

  const drawOnCanvas = () => {
    if (canvasRef.current) {
      const points = waveForm.length;
      const ctx = canvasRef.current.getContext('2d');


      const { width: w, height: h } = canvasRef.current.getBoundingClientRect();
      const baseLineY = h - h / 6;
      const maxY = h / 2;
      const segWidth = w / (points);

      ctx.clearRect(0, 0, w, h);
      ctx.moveTo(0, baseLineY);
      ctx.lineWidth = 4;
      if (points > 0) {
        for (let i = 0; i < points; i += 1) {
          if (!isPlaying) {
            ctx.strokeStyle = theme.palette.secondary.main;
          } else if (percent && i < percent * points) ctx.strokeStyle = theme.palette.primary.main;
          else {
            ctx.strokeStyle = theme.palette.secondary.main;
          }
          const x = i * segWidth;
          const y = baseLineY - maxY * waveForm[i] - 2;
          ctx.beginPath();
          ctx.moveTo(x, baseLineY);
          ctx.lineTo(x, y);
          ctx.stroke();
          ctx.closePath();
        }
      } else if (isPlaying && percent) {
        const py = 12;
        const gradient = ctx.createLinearGradient(0, 0, w * percent, 0);
        gradient.addColorStop(0, '#00000000');
        gradient.addColorStop(1, theme.palette.primary.dark);
        ctx.fillStyle = gradient;
        ctx.fillRect(0, py, w * percent, h - (py * 2));
      }
    }
  };

  const handleSeek = (e) => {
    e.stopPropagation();
    if (isPlaying) {
      const rect = e.target.getBoundingClientRect();
      const x = e.clientX - rect.left;
      const cTime = (x / 500) * duration;
      dispatch(setPlayerSeekCurrentTime(cTime));
    }
  };

  useEffect(() => {
    if (isPlaying) {
      dispatch(setPlayerCurrentTimeHandler(setCurrentTime));
    }
  }, [dispatch, isPlaying]);

  useEffect(() => {
    drawOnCanvas();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [waveForm, isPlaying, percent]);

  return (
    <canvas
      className={classes.canvas}
      ref={canvasRef}
      width={500}
      height={32}
      onClick={handleSeek}
    />
  );
};

export default WaveFormVisualzer;
