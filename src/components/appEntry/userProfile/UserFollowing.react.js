/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
import React, { useEffect } from 'react';
import {
  Grid, Typography, Avatar,
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useHistory } from 'react-router-dom';
import NeuButton from '../../neumorphism/NeuButton.react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import { getVisitedUserFollowing, getNextVisitedUserFollowing } from '@actions/visitedUserActionCreator';
import { followUser, unfollowUser } from '@actions/followActionCreator';


const UserFollowing = () => {
  const useStyles = makeStyles(() => ({
    border: {
      borderBottomColor: 'white',
      paddingTop: '4px',
    },
    following: {
      boxShadow: 'none',
      borderRadius: '10px',
      backgroundColor: 'rgba(255, 17, 133, 0.3)',
      '&:hover': {
        boxShadow: 'none',
        borderRadius: '10px',
        backgroundColor: 'rgba(255, 17, 133, 0.3)',
      },
    },
    otherUsers: {
      borderBottom: '2px',
    },
    followStatus: {
      background: 'gray',
      opacity: '0.5',
    },
    backIcon: {
      cursor: 'pointer',
    },
    options: {
      cursor: 'pointer',
    },
  }));

  const classes = useStyles();
  const currentUser = useSelector((state) => state.currentUser.currentUser);
  const visitedUser = useSelector((state) => state.visitedUser.visitedUser);
  const visitedUserFollowing = useSelector((state) => state.visitedUser.visitedUserFollowing);
  const currentUserFollowers = useSelector((state) => state.currentUser.currentUserFollowers);
  const currentUserFollowing = useSelector((state) => state.currentUser.currentUserFollowing);
  const nextFollowingPage = useSelector((state) => state.visitedUser.nextFollowingPage);
  const history = useHistory();
  const dispatch = useDispatch();

  const unfollow = (id) => {
    dispatch(unfollowUser({ visitedUser: id, currentUser: currentUser.id }));
  };

  const follow = (id) => {
    dispatch(followUser({ visitedUser: id, currentUser: currentUser.id }));
  };

  const showMoreUsers = () => {
    dispatch(getNextVisitedUserFollowing(nextFollowingPage));
  };


  useEffect(() => {
    dispatch(getVisitedUserFollowing(visitedUser.id));
  }, [dispatch, visitedUser.id]);
  return (
    <Grid container direction="column" justify="flex-start" alignItems="stretch">
      <Grid item xs>
        <Box borderBottom={1} className={classes.border}>
          <Grid item xs container direction="row" justify="flex-start" alignItems="stretch">
            <Grid item xs={2} container direction="row" justify="center" alignItems="center" className={classes.backIcon}>
              <ArrowBackIcon color="primary" onClick={() => history.goBack()} className={classes.backIcon} />
            </Grid>
            <Grid item xs container direction="column" justify="flex-start" alignItems="stretch">
              <Grid item xs>
                <Typography color="textPrimary" variant="h4">{visitedUser.nickname}</Typography>
              </Grid>
              <Grid item xs>
                <Typography color="textPrimary" variant="h6">{visitedUser.username}</Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs container direction="row" justify="center" alignItems="stretch">
            <Grid item xs>
              <NeuButton fullWidth onClick={() => history.push(`/${visitedUser.username}/followers`)} textColor="primary">followers</NeuButton>
            </Grid>
            <Grid item xs>
              <NeuButton fullWidth onClick={() => history.push(`/${visitedUser.username}/following`)} textColor="primary" className={classes.following}>following</NeuButton>
            </Grid>
          </Grid>
        </Box>
      </Grid>
      <Grid item xs container direction="column" justify="flex-start" alignItems="stretch">
        <Grid item xs className={classes.otherUsers}>
          {visitedUserFollowing.map((following) => (
            <Box borderBottom={1} className={classes.border} key={following.username} paddingY={2}>
              <Grid item xs container dircetion="row" justify="flex-start" alignItems="stretch">
                <Grid item xs={1} container direction="column" justify="center" alignItems="center">
                  <Avatar alt={following.nickname} src={following.avatar} />
                </Grid>
                <Grid item xs container direction="column" justify="flex-start" alignItems="stretch">
                  <Grid item xs container direction="row" justify="flex-start" alignItems="stretch">
                    <Grid item xs container direction="column" justify="flex-start" alignItems="stretch" onClick={() => (history.push(`/${following.username}`))} className={classes.options}>
                      <Grid item xs>
                        <Typography color="textPrimary">{following.nickname}</Typography>
                      </Grid>
                      <Grid item xs container direction="row" justify="flex-start" alignItems="center" spacing={0}>
                        <Grid item xs>
                          <Typography color="textSecondary">{`@${following.username}`}</Typography>
                        </Grid>
                        <Grid item xs>
                          {
                                currentUserFollowers.filter((users) => (users.username === following.username)).length !== 0
                                  ? <Typography color="textSecondary" variant="caption" className={classes.followStatus}>Follows You</Typography> : <div> </div>
                          }
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs container direction="column" justify="flex-end" alignItems="flex-end">
                      {
                        currentUserFollowing.filter((users) => (users.username === following.username)).length !== 0
                          ? <NeuButton textColor="primary" onClick={() => unfollow(following.id)}>Following</NeuButton>
                          : currentUserFollowers.filter((users) => (users.username === following.username)).length !== 0
                            ? <NeuButton textColor="primary" onClick={() => follow(following.id)}>Follow Back</NeuButton>
                            : currentUser.username !== following.username
                              ? <NeuButton textColor="primary" onClick={() => follow(following.id)}>Follow</NeuButton>
                              : <div> </div>
                      }
                    </Grid>
                  </Grid>
                  <Grid item xs>
                    <Typography variant="body2" color="textPrimary">{following.about}</Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          ))}
          {
            nextFollowingPage ? <NeuButton textColor="primary" fullWidth onClick={showMoreUsers}>Show more</NeuButton> : <div> </div>
          }
        </Grid>
      </Grid>
    </Grid>
  );
};

export default UserFollowing;
