import React from 'react';
import { TextField, makeStyles, useTheme } from '@material-ui/core';
import * as PropTypes from 'prop-types';
import Color from 'color';

const useStyles = makeStyles((theme) => ({
  root: (props) => ({
    backgroundColor: `${props.color} !important`,
    boxShadow: `inset -${props.offset}px -${props.offset}px ${props.spread}px ${props.lightColor}
    ,inset ${props.offset}px ${props.offset}px ${props.spread}px ${props.darkColor}`,
    borderRadius: props.borderRadius,
    transition: `box-shadow ${theme.transitions.duration.standard}ms ${theme.transitions.easing.easeInOut}`,
    '&:hover': {
      boxShadow: `inset -${props.offset * 2}px -${props.offset * 2}px ${props.spread}px ${props.lightColor}
      ,inset ${props.offset * 2}px ${props.offset * 2}px ${props.spread}px ${props.darkColor}`,
    },
    '&:active': {
      boxShadow: `inset -${props.offset * 2}px -${props.offset * 2}px ${props.spread}px ${props.lightColor}
      ,inset ${props.offset * 2}px ${props.offset * 2}px ${props.spread}px ${props.darkColor}`,
    },
    '&:focus': {
      boxShadow: `inset -${props.offset * 2}px -${props.offset * 2}px ${props.spread}px ${props.lightColor}
      ,inset ${props.offset * 2}px ${props.offset * 2}px ${props.spread}px ${props.darkColor}`,
    },
  }),
  select: {
    '&:focus': {
      backgroundColor: 'unset',
    },
  },
}));

const NeuTextField = ({
  elevation = 1, borderRadius = 2, InputProps, SelectProps, ...otherProps
}) => {
  const theme = useTheme();
  const offset = elevation * 2;
  const spread = elevation * 6;
  const color = theme.palette.background.default;
  const lightColor = new Color(color).lighten(0.6).fade(0.3).rgb()
    .string();
  const darkColor = new Color(color).darken(0.6).fade(0.6).rgb()
    .string();

  const classes = useStyles({
    borderRadius: (theme.shape.borderRadius * borderRadius * 2),
    offset,
    spread,
    lightColor,
    color,
    darkColor,
  });
  const internalInputProps = {
    ...InputProps,
    disableUnderline: true,
    classes: {
      ...InputProps?.classes,
      root: classes.root,
    },
  };
  const internalSelectProps = {
    ...SelectProps,
    classes: {
      ...SelectProps?.classes,
      root: classes.select,
    },
  };
  return (
    <TextField
      size="small"
      {...otherProps}
      variant="filled"
      InputProps={internalInputProps}
      SelectProps={internalSelectProps}
    />
  );
};

NeuTextField.propTypes = {
  elevation: PropTypes.number,
  borderRadius: PropTypes.number,
  variant: PropTypes.string,
  InputProps: PropTypes.object,
  SelectProps: PropTypes.object,
};

export default NeuTextField;
