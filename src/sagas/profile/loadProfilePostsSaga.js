import {
  takeLatest, call, put,
} from 'redux-saga/effects';
import * as Api from '@api';
import { loadProfilePosts, setProfilePosts, setProfileNextPageUrl } from '@actions/profileActionCreator';

// not finished
function* worker({ payload: userId }) {
  try {
    const {
      data: {
        // eslint-disable-next-line no-unused-vars
        count, next, previous, results,
      },
    } = yield call(Api.getUserPosts, userId);

    yield put(setProfilePosts(results));
    yield put(setProfileNextPageUrl(next));
  } catch (e) {
    console.log('error', e);
  }
}

export default function* loadProfilePostsSaga() {
  yield takeLatest(loadProfilePosts, worker);
}
