import React from 'react';
import VerificationModal from './VerificationModal.react';
import ReplyToPostModal from './ReplyToPostModal.react';
import LikerListModal from './LikerList.react';

const Modals = () => (
  <>
    <VerificationModal />
    <ReplyToPostModal />
    <LikerListModal />
  </>
);

export default Modals;
