import { put, call, takeLatest } from 'redux-saga/effects';
import { setAuthToken, setCurrentUser } from '@actions/currentUserActionCreator';
import { clearToken } from '@api';
import { setIsLoggedIn, setIsUserVerified } from '@actions/appStateActionCreator';
import { signOut } from '@actions/loginActionCreator';
import * as Api from '@api';

function* signOutWorkerSaga() {
  try {
    yield call(clearToken);
    yield put(setIsLoggedIn(false));
    yield put(setIsUserVerified(false));
    yield put(setAuthToken(null));
    yield put(setCurrentUser(null));
    yield call(Api.signOut);
  } catch (e) {

  }
}


export default function* signOutSaga() {
  yield takeLatest(signOut, signOutWorkerSaga);
}
