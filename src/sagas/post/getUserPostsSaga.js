import { takeEvery, call } from 'redux-saga/effects';
import { getUserPosts } from '@actions/postActionCreator';
import * as Api from '@api';

// not finished
function* worker({ payload }) {
  try {
    const results = yield call(Api.getUserPosts, payload);
  } catch (e) {

  }
}

export default function* getUserPostsSaga() {
  yield takeEvery(getUserPosts, worker);
}
