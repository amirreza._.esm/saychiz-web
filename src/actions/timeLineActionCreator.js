import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  TIMELINE_SET_POSTS,
  TIMELINE_APPEND_POSTS,
  TIMELINE_SET_NEXT_PAGE_URL,
  SAGA_LOAD_TIMELINE_POSTS,
  SAGA_TIMELINE_LOAD_MORE,
} = ActionTypes;

export const setTimelinePosts = createAction(TIMELINE_SET_POSTS);
export const appendTimelinePosts = createAction(TIMELINE_APPEND_POSTS);
export const setTimelineNextPageUrl = createAction(TIMELINE_SET_NEXT_PAGE_URL);
export const loadTimelinePosts = createAction(SAGA_LOAD_TIMELINE_POSTS);
export const timelineLoadMore = createAction(SAGA_TIMELINE_LOAD_MORE);
