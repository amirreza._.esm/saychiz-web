import React, { useRef, useEffect } from 'react';
import { Box, makeStyles } from '@material-ui/core';
import Surface from './common/Surface.react';
import { useSelector, useDispatch } from 'react-redux';
import { appConfig } from '@constants';
import Visualizer from './postComponents/Visualizer.react';
import { setPlayerIsPlaying } from '@actions/audioPlayerActionCreator';
import NeuIconButton from './neumorphism/NeuIconButton.react';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';

const useStyles = makeStyles((theme) => ({
  playBtn: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  },
}));


const AudioPlayer = () => {
  const audioRef = useRef();
  const analyserRef = useRef();

  const classes = useStyles();
  const dispatch = useDispatch();

  // const url = useSelector((state) => state.audioPlayer.url);
  const postId = useSelector((state) => state.audioPlayer.postId);
  const isPlaying = useSelector((state) => state.audioPlayer.isPlaying);
  const setCaller = useSelector((state) => state.audioPlayer.currentTimeHandler);
  const seekCurrentTime = useSelector((state) => state.audioPlayer.seekCurrentTime);

  useEffect(() => {
    const audio = audioRef.current;
    if (audio) {
      if (seekCurrentTime > 0 && seekCurrentTime < audio.duration) {
        audio.currentTime = seekCurrentTime;
      }
    }
  }, [seekCurrentTime]);

  useEffect(() => {
    const audio = audioRef.current;
    if (audio) {
      if (postId) {
        audio.src = `${appConfig.serverBaseUrl}post/streaming-file/${postId}/`;
        audio.load();
        audio.play();
      }
    }
  }, [postId]);

  useEffect(() => {
    const audio = audioRef.current;
    if (audio) {
      if (isPlaying) {
        audio.play();
      } else {
        audio.pause();
      }
    }
  }, [isPlaying]);

  useEffect(() => {
    const audio = audioRef.current;

    audio.ontimeupdate = () => {
      if (typeof setCaller === 'function') {
        setCaller(audio.currentTime);
      }
    };
  }, [setCaller]);

  useEffect(() => {
    const audio = audioRef.current;
    const audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    const source = audioCtx.createMediaElementSource(audio);
    const analyser = audioCtx.createAnalyser();
    source.connect(analyser);
    analyserRef.current = analyser;
    analyser.fftSize = 2048;
    source.connect(audioCtx.destination);
  }, []);

  return (
    <Box
      position="absolute"
      bottom={64}
    >
      {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
      <audio
        crossOrigin="anonymous"
        ref={audioRef}
        onEnded={() => dispatch(setPlayerIsPlaying(false))}
      />
      <Surface>
        <Visualizer analyserRef={analyserRef} isPlaying={isPlaying} />
      </Surface>
      <Box
        position="absolute"
        marginTop={-2}
        marginLeft={-2}
        top="50%"
        left="50%"
      >
        <NeuIconButton
          disabled={!postId}
          className={classes.playBtn}
          elevation={0.5}
          textColor="primary"
          onClick={() => dispatch(setPlayerIsPlaying(!isPlaying))}
        >
          {isPlaying ? <PauseIcon /> : <PlayArrowIcon />}
        </NeuIconButton>
      </Box>
    </Box>
  );
};

export default AudioPlayer;
