import { handleActions } from 'redux-actions';
import { openLikerListModal, closeLikerListModal, setLikerList } from '@actions/likerListModalActionCreator';

const initialState = {
  isOpen: false,
  likerList: [],
};

export default handleActions({
  [openLikerListModal]: (state) => ({ ...state, isOpen: true }),
  [closeLikerListModal]: (state) => ({ ...state, isOpen: false }),
  [setLikerList]: (state, { payload }) => ({ ...state, likerList: payload }),
}, initialState);
