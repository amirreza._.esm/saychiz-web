import { createAction } from 'redux-actions';
import { ActionTypes } from '@constants';

const {
  CURRENT_USER_SET_CURRENT_USER,
  CURRENT_USER_REMOVE_CURRENT_USER,
  CURRENT_USER_SET_AUTH_TOKEN,
  CURRENT_USER_REMOVE_AUTH_TOKEN,
  CURRENT_USER_REMOVE_CURRENT_USER_FOLLOWERS,
  CURRENT_USER_REMOVE_CURRENT_USER_FOLLOWING,
  CURRENT_USER_SET_CURRENT_USER_FOLLOWERS,
  CURRENT_USER_SET_CURRENT_USER_FOLLOWING,
  CURRENT_USER_ADD_CURRENT_USER_FOLLOWERS,
  CURRENT_USER_ADD_CURRENT_USER_FOLLOWING,
  CURRENT_USER_SET_NEXT_FOLLOWING,
  CURRENT_USER_SET_NEXT_FOLLOWERS,
  CURRENT_USER_REMOVE_USER_FOLLOWER,
  CURRENT_USER_REMOVE_USER_FOLLOWING,
  SAGA_CURRENT_USER_GET_ALL_USER_FOLLOWRS,
  SAGA_CURRENT_USER_GET_ALL_USER_FOLLOWING,
} = ActionTypes;

export const setCurrentUser = createAction(CURRENT_USER_SET_CURRENT_USER);
export const removeCurrentUser = createAction(CURRENT_USER_REMOVE_CURRENT_USER);
export const setAuthToken = createAction(CURRENT_USER_SET_AUTH_TOKEN);
export const removeAuthToken = createAction(CURRENT_USER_REMOVE_AUTH_TOKEN);
export const removeCurrentUserFollowers = createAction(CURRENT_USER_REMOVE_CURRENT_USER_FOLLOWERS);
export const removeCurrentUserFollowing = createAction(CURRENT_USER_REMOVE_CURRENT_USER_FOLLOWING);
export const setCurrentUserFollowers = createAction(CURRENT_USER_SET_CURRENT_USER_FOLLOWERS);
export const setCurrentUserFollowing = createAction(CURRENT_USER_SET_CURRENT_USER_FOLLOWING);
export const addCurrentUserFollowers = createAction(CURRENT_USER_ADD_CURRENT_USER_FOLLOWERS);
export const addCurrentUserFollowing = createAction(CURRENT_USER_ADD_CURRENT_USER_FOLLOWING);
export const setNextFollowing = createAction(CURRENT_USER_SET_NEXT_FOLLOWING);
export const setNextFollowers = createAction(CURRENT_USER_SET_NEXT_FOLLOWERS);
export const removeUserFollower = createAction(CURRENT_USER_REMOVE_USER_FOLLOWER);
export const removeUserFollowing = createAction(CURRENT_USER_REMOVE_USER_FOLLOWING);

export const getAllCurrentUserFollowers = createAction(SAGA_CURRENT_USER_GET_ALL_USER_FOLLOWRS);
export const getAllCurrentUserFollowing = createAction(SAGA_CURRENT_USER_GET_ALL_USER_FOLLOWING);
