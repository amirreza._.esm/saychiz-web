import { ActionTypes } from '@constants';
import { createAction } from 'redux-actions';

const {
  APP_STATE_SET_IS_INITIATED,
  APP_STATE_SET_IS_LOGGED_IN,
  APP_STATE_SET_IS_USER_VERIFIED,
  APP_STATE_SET_APP_PAGE,
} = ActionTypes;

export const setIsInitiated = createAction(APP_STATE_SET_IS_INITIATED);
export const setIsLoggedIn = createAction(APP_STATE_SET_IS_LOGGED_IN);
export const setIsUserVerified = createAction(APP_STATE_SET_IS_USER_VERIFIED);

export const setAppPage = createAction(APP_STATE_SET_APP_PAGE);
