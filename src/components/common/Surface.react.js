import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core';
import Color from 'color';
import * as PropTypes from 'prop-types';
import { motion } from 'framer-motion';

const useStyles = makeStyles((theme) => ({
  surface: (props) => ({
    backgroundColor: theme.palette.background.default,
    borderRadius: `${props.borderRadius}px`,
  }),
}));

const Surface = ({
  elevation = 1, borderRadius = 2, children, startUp, className, ...otherProps
}) => {
  const theme = useTheme();
  const classes = useStyles({ borderRadius: (theme.shape.borderRadius * borderRadius * 2) });
  const offset = elevation * 3;
  const spread = elevation * 10;
  const lightColor = new Color(theme.palette.background.default).lighten(0.6).fade(0.2).rgb()
    .string();
  const darkColor = new Color(theme.palette.background.default).darken(0.6).fade(0.6).rgb()
    .string();

  return (
    <motion.div
      className={`${classes.surface} ${className}`}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...(!startUp && { initial: false })}
      animate={{
        boxShadow: `-${offset}px -${offset}px ${spread}px ${lightColor}
        , ${offset}px ${offset}px ${spread}px ${darkColor}`,
      }}
      transition={{ duration: 0.2 }}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...otherProps}
    >
      {children}
    </motion.div>
  );
};

Surface.propTypes = {
  elevation: PropTypes.number,
  borderRadius: PropTypes.number,
  startUp: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.any,
};

export default Surface;
